#!/usr/bin/perl

#----------------------------------------------------------------------
#
# digikam2web.pl -- Torsten Crass, 2007 - 2012
#
# A versatile Perl script for creating themable HTML galleries
# from DigKam albums.
#
# Call 'digikam2web.pl --help' for more information.
#
# This program's homepage: http://www.tcrass.de/computer/digikam2web
#
# DigiKam homepage: http://www.digikam.org
#
# LICENSE
# -------
#
# This program is distributed under the terms of the GNU GPL,
# version 2 (see http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
#
#----------------------------------------------------------------------


use strict;
use warnings;
use utf8;
use open IN => ':utf8', OUT => ':utf8';
use Encode;
use feature 'unicode_strings';

#binmode STDIN, ':encoding(UTF-8)'; binmode STDOUT, ':encoding(UTF-8)';

use SysUtils;


#=== Global constants and command-line options ========================

my $VERSION = '1.0.6';

my @VALID_EXTENSIONS;      # Combined extensions for images and videos
my $ALL_ALBUMS_INDEX;      # album_id => album
my $ALL_PHOTOS_INDEX = {}; # image_id => image
my $CROP_X;                # Cropping dimensions for thumbnails;
my $CROP_Y;                #    applied after downscaling
my @INCLUDE_TAG_IDS = ();  # Only include images having any of these
                           # tag ids assigend to them
my @EXCLUDE_TAG_IDS = ();  # Exclude all images having any of these
                           # tag ids assigend to them
# List of database fields to be sorted numerically
# rather than lexically
my %SORT_NUMERIC = (
  album => 1,
  albumRoot => 1,
  category => 1,
  colorDepth => 1,
  colorModel => 1,
  dirid => 1,
  fileSize => 1,
  height => 1,
  icon => 1,
  id => 1,
  orientation => 1,
  rating => 1,
  status => 1,
  width => 1
);

# Global variables to take up command line settings
my $ALBUM_TEMPLATE_FILE = undef;
my $DEBUG = 0;
my $DB_VERSION = 4;
my @INCLUDE_COLLECTIONS = ();
my @EXCLUDE_COLLECTIONS = ();
my $DIGIKAM_ROOT = "$SysUtils::HOMEDIR${SysUtils::DIRSEP}My Photos";
my $DIGIKAM_DB = undef;
my @PHOTO_EXTENSIONS = qw(jpg jpeg png tif tiff);
my @MOVIE_EXTENSIONS = qw(avi flv mov mpeg mpg ogg wmv);
my $FLATTEN_PHOTOS = 0;
my $FLATTEN_ALBUMS = 0;
my @INCLUDE_TAGS = ();
my @EXCLUDE_TAGS = ();
my $HOME_STRING = 'Start';
my $IMAGE_SIZE = '960x576';
my $THUMBNAIL_SIZE = '128x128';
my $SKIP_EMPTY_ALBUMS = 0;
my $INCLUDE_MOVIES = 0;
my $INCLUDE_MOVIE_ORIGS = 0;
my $DONT_RECREATE = 0;
my $ONLY_NEW_PHOTOS = 0;
my $INCLUDE_ORIGS = 0;
my $CROP_THUMBNAILS = '96x96';
my $PHOTO_TEMPLATE_FILE = undef;
my $QUALITY = 90;
my $THUMBNAIL_QUALITY = 80;
my $RECURSE = 0;
my $MIN_RATING = 0;
my $SORT_PHOTOS_BY = 'name';
my $SORT_ALBUMS_BY = 'name';
my $TARGET_DIR = "$SysUtils::HOMEDIR${SysUtils::DIRSEP}gallery";
my $DATE_FORMAT = '%F';
my $USE_SRGB_PROFILE = 0;
my $SRGB_PROFILE_PATH = '/usr/share/color/icc/sRGB.icc';
my $VERBOSE = 0;
my $WIN_AUTORUN = '';
my @EXCLUDE_PHOTOS = ();
my @EXCLUDE_ALBUMS = ();
my $STYLESHEET_FILE = undef;

# Parse and process command line options
(my $opts, my $args, my $err) = &SysUtils::parseOptions({
  'help|?' => 0,
  'albums-template|a=' => \$ALBUM_TEMPLATE_FILE,
  'print-album-template|A' => 0,
  'debug|b' => \$DEBUG,
  'database-version|B=' => \$DB_VERSION,
  'include-collection|c@' => \@INCLUDE_COLLECTIONS,
  'exclude-collection|C@' => \@EXCLUDE_COLLECTIONS,
  'digikam-root|d=' => \$DIGIKAM_ROOT,
  'digikam-db|D=' => \$DIGIKAM_DB,
  'image-extension|e@' => \@PHOTO_EXTENSIONS,
  'movie-extension|E@' => \@MOVIE_EXTENSIONS,
  'flatten-photos|f' => \$FLATTEN_PHOTOS,
  'flatten-albums|F' => \$FLATTEN_ALBUMS,
  'include-tag|g@' => \@INCLUDE_TAGS,
  'exclude-tag|G@' => \@EXCLUDE_TAGS,
  'home-string|h=' => \$HOME_STRING,
  'image-size|i=' => \$IMAGE_SIZE,
  'thumbnail-size|I=' => \$THUMBNAIL_SIZE,
  'skip-empty-albums|k' => \$SKIP_EMPTY_ALBUMS,
  'include-movies|m' => \$INCLUDE_MOVIES,
  'include-movie-origs|M' => \$INCLUDE_MOVIE_ORIGS,
  'dont-recreate|n' => \$DONT_RECREATE,
  'only-new-photos|N' => \$ONLY_NEW_PHOTOS,
  'include-origs|o' => \$INCLUDE_ORIGS,
  'crop-thumbnails|O=' => \$CROP_THUMBNAILS,
  'photo-template|p=' => \$PHOTO_TEMPLATE_FILE,
  'print-photo-template|P' => 0,
  'quality|q=' => \$QUALITY,
  'thumbnail-quality|Q=' => \$THUMBNAIL_QUALITY,
  'recurse|r' => \$RECURSE,
  'min-rating|R=' => \$MIN_RATING,
  'sort-photos-by|s=' => \$SORT_PHOTOS_BY,
  'sort-albums-by|S=' => \$SORT_ALBUMS_BY,
  'target-dir|t=' => \$TARGET_DIR,
  'date-format|T=' => \$DATE_FORMAT,
  'use-srgb-profile|u' => \$USE_SRGB_PROFILE,
  'srgb-profile-path|U=' => \$SRGB_PROFILE_PATH,
  'verbose|v' => \$VERBOSE,
  'version|V' => 0,
  'win-autorun|w=' => \$WIN_AUTORUN,
  'exclude-photos|x@' => \@EXCLUDE_PHOTOS,
  'exclude-albums|X@' => \@EXCLUDE_ALBUMS,
  'stylesheet|y=' => \$STYLESHEET_FILE,
  'print-stylesheet|Y' => 0
}, -1);
# Note: still unused one-letter options are HjJKlLuUWzZ

if ($err) {
  print("$err\n");
&printUsage();
  die();
}
if ($opts->{help}) {
  &printUsage();
  exit(0);
}
if ($opts->{version}) {
  print "$VERSION\n";
  exit(0);
}
if ($opts->{'print-album-template'}) {
  print(&getDefaultAlbumTemplate());
  exit(0);
}
if ($opts->{'print-photo-template'}) {
  print(&getDefaultPhotoTemplate());
  exit(0);
}
if ($opts->{'print-stylesheet'}) {
  print(&getDefaultStylesheet());
  exit(0);
}
if (scalar(@{$args}) == 0) {
  print "You need to specify at least one album!\n";
  &printUsage();
  exit(0);
}

# Remove trailing directory separator, if given
$DIGIKAM_ROOT = &SysUtils::cleanDir($DIGIKAM_ROOT);
$TARGET_DIR = &SysUtils::cleanDir($TARGET_DIR);

# Check for valid database version
unless (($DB_VERSION == 3) || ($DB_VERSION == 4)) {
  die("$DB_VERSION is not a valid database version!\n");
}
# Set default digikam db, if not specified
unless ($DIGIKAM_DB) {
  $DIGIKAM_DB = "$DIGIKAM_ROOT${SysUtils::DIRSEP}digikam$DB_VERSION.db";
}

# Check for presence of specified digikam db file
unless (-e $DIGIKAM_DB) {
  die("Unable to locate digikam db file '$DIGIKAM_DB'!\n");
}
# Including movie files implies including movies
if ($INCLUDE_MOVIE_ORIGS) {
  $INCLUDE_MOVIES = $INCLUDE_MOVIE_ORIGS;
}
# Build list of valid file extensions
@VALID_EXTENSIONS = @PHOTO_EXTENSIONS;
if ($INCLUDE_MOVIES) {
  push(@VALID_EXTENSIONS, @MOVIE_EXTENSIONS);
}

#=== Start of main program ============================================

# Get default templates
my $albumTemplate = &removeHTMLComments(&getDefaultAlbumTemplate());
my $photoTemplate = &removeHTMLComments(&getDefaultPhotoTemplate());
my $stylesheet = &getDefaultStylesheet();

# Find list of albums to include into gallery
my $wantedAlbums = &getAlbums($args,
                              \@EXCLUDE_ALBUMS,
                              \@INCLUDE_COLLECTIONS,
                              \@EXCLUDE_COLLECTIONS);
# This will become the root of the selected album's tree
my $wantedRoot;

if (scalar(@{$wantedAlbums}) == 0) {
  print("No album found matching your request!\n");
  exit(0);
}

# We need the relationshipt between all albums in order
# to build a sensible tree for the selected albums
&report("Retrieving album information...\n");
my $allAlbums = &getAlbums();
# Build an album_id => album-hashref index
$ALL_ALBUMS_INDEX = &getAlbumIndex($allAlbums, 'id');
# Build the complete album tree
&makeTrees($allAlbums);
my $allRoot = &makeCommonRoot($allAlbums);

# If requested, add sub-albums of selected albums
if ($RECURSE) {
  &addSubAlbums($wantedAlbums, $allRoot);
  # Build tree relationships between selected albums
  &makeTrees($wantedAlbums);
}

# There shall not be any nested album pages; rather,
# all selected albums' pages are to be sub-pages
# of a single root page
if ($FLATTEN_ALBUMS) {
  # If there's only one album, use it as root
  if (scalar(@{$wantedAlbums}) == 1) {
    $wantedRoot = $wantedAlbums->[0];
  }
  # Otherwise, subsume all albums under a common root
  else {
    $wantedRoot = &makeFlatRoot($wantedAlbums);
  }
}
# All photos shall be displayed in one common album page
elsif ($FLATTEN_PHOTOS) {
  # If there's only on album, use it as root
  if (scalar(@{$wantedAlbums}) == 1) {
    $wantedRoot = &makeClone($wantedAlbums->[0]);
  }
  # Otherwise, create a new empty root album
  else {
    $wantedRoot = &makeEmptyRoot();
  }
}
# Each selected album sub-trees shall be subsumed as-is
# under a common root
else {
  $wantedRoot = &makeCommonRoot($wantedAlbums)
}

# Retrieve information on photos in selected albums
&report("Retrieving photo information...\n");
# Add photos to albums
&getTagIds();
&addPhotos($wantedRoot, $wantedAlbums);

# Remove empty albums from the tree, if requested
if ($SKIP_EMPTY_ALBUMS) {
  &removeEmptyAlbums($wantedRoot);
  $wantedAlbums = &getAlbumsInTree($wantedRoot);
}

# Add HTML-related information, like nesting
# level, URL of parent, relative URL to root etc.
&addAlbumWebInfo($wantedRoot, 0);
&addPhotoWebInfo($wantedRoot, $wantedAlbums);

# Sort photo entries
&sortPhotos($wantedRoot);

# Build the target directories basic subdirectory
# structure and write static files
if ($STYLESHEET_FILE) {
  (my $ref, my $err) = &SysUtils::readFromFile($STYLESHEET_FILE);
  if ($err) {
    die($err);
  }
  $stylesheet = ${$ref}
}
&prepareTargetDir($stylesheet);

# Build HMTL pages for photos...
&report("Creating HTML pages...\n");
if ($PHOTO_TEMPLATE_FILE) {
  (my $ref, my $err) = &SysUtils::readFromFile($PHOTO_TEMPLATE_FILE);
  if ($err) {
    die($err);
  }
  $photoTemplate = &removeHTMLComments(${$ref});
}
my $parsedPhotoTemplate = &parsePhotoTemplate($photoTemplate);
&makePhotoPages($wantedRoot, $parsedPhotoTemplate);
# ...and albums
if ($ALBUM_TEMPLATE_FILE) {
  (my $ref, my $err) = &SysUtils::readFromFile($ALBUM_TEMPLATE_FILE);
  if ($err) {
    die($err);
  }
  $albumTemplate = &removeHTMLComments(${$ref});
}
my $parsedAlbumTemplate = &parseAlbumTemplate($albumTemplate);
&makeAlbumPage($wantedRoot, $parsedAlbumTemplate);

# Determine how to crop thumbnails after downsizing
if ($CROP_THUMBNAILS) {
  my @dims = split(/x/, $CROP_THUMBNAILS);
  if (scalar(@dims) == 0 or scalar(@dims) > 2) {
    die("Ill-formatted cropping specification: $CROP_THUMBNAILS");
  }
  foreach my $dim (@dims) {
    unless ($dim =~ m/^\d+$/) {
      die("Ill-formatted croppng dimension specification: $dim");
    }
  }
  $CROP_X = $dims[0];
  if (scalar(@dims) == 1) {
    $CROP_Y = $CROP_X
  }
  else {
    $CROP_Y = $dims[1];
  }
}
# Process (baiscally: downscale) photo (and video) files
&makePhotos($wantedRoot);
# Create sub-albumn and photo thumbnails
&makeAlbumThumbs($wantedRoot);

# If creation of Windows AUTORUN.INF file is requested...
if ($WIN_AUTORUN) {
  # Open 'index.html' in default browser
  my $autorun = "[autorun]\r\nshellexecute=index.html\r\n";
  # Use given string as drive label
  $autorun .= "label=$WIN_AUTORUN\r\n";
  # Create AUTORUN.INF file
  my $err = &SysUtils::writeToFile("$TARGET_DIR${SysUtils::DIRSEP}AUTORUN.INF", $autorun);
  if ($err) {
    print("Error while creating Windows AUTORUN.INF file: $err");
  }
}

#=== End of main program ==================================


#--- Usage hint
#
sub printUsage {
  print <<EOT;

digikam2web.pl - Create HTML galleries from digkam albums
Version $VERSION
-----------------------------------------------------------------------
Usage: digikam2web.pl [options] <album_pattern> [<album_pattern>...]
with possible options:
-?|--help                   : Print this message and exit.
-a|--album-template <path>  : Use <path> as HTML template for albums.
-A|--print-album-template   : Prints out the internal album template
                              and exits.
-b|--debug                  : Prints out all SQL queries issued
                              during gallery creation.
-B|--db-version             : digiKam database schema version; must
                              be either 3 or 4. [$DB_VERSION]
-c|--include-collection <patt>:  Only include albums  belonging to a
                              collection matching the given SQL
                              pattern. May be specified multiple times.
-C|--exclude-collection <patt>:  Exclude all albums belonging to any
                              collection matching the given SQL
                              pattern. May be specified multiple times.
-d|--digikam-root <dir>     : Path to digiKam root directory.
                              [$DIGIKAM_ROOT]
-D|--digikam-db <path>      : Path to digikam.db; assumed to reside in
                              digikam root, if not specified otherwise.
-e|--image-extension <ext>  : Extension of image files to be included.
                              May be specified multiple times; if
                              specified, the default list of image
                              file extensions will be overwritten.
                              [@PHOTO_EXTENSIONS]
-E|--movie-extension <ext>  : Same as -e, but for movie files.
                              [@MOVIE_EXTENSIONS]
-f|--flatten-photos         : Put photos of all selected albums into
                              one root album.
-F|--flatten-albums         : Treat all albums as sub-albums of a
                              common root album, disregarding the
                              original album tree.
-g|--include-tag <patt>     : Only include photos being tagged with
                              a tag matching the given SQL pattern
                              (or any of its sub-tags). May be
                              specified multiple times.
-G|--exclude-tag <patt>     : Exclude all photos being tagged with
                              a tag matching the given SQL pattern
                              (or any of its sub-tags). May be
                              specified multiple times.
-h|--home-string <string>   : Use <string> as name for the root album,
                              unless it does have a name or a caption
                              on its own. [$HOME_STRING]
-i|--image-size <size>      : Desired photo size in ImageMagick format.
                              [$IMAGE_SIZE]
-I|--thumbnail-size <size>  : Desired size for thumbnails (in
                              ImageMagick format). [$THUMBNAIL_SIZE]
-k|--skip-empty-albums      : Do not include albums which do not
                              contain any photos.
-m|--include-movies         : Include movies into gallery, each being
                              represented by a static image of its
                              first frame. Requires ffmpeg.
-M|--include-movie-origs    : Include links to original movie files.
                              Implies -m
-n|--dont-recreate          : Don't recreate image files, but just
                              the gallery's directory structure and
                              HTML files.
-N|--only-new-photos        : Only process images which are not already
                              present in the gallery. Useful for adding
                              new photos to an existing gallery.
-o|--include-origs          : Include links to the original photo
                              files. Does not include movies; use -M,
                              if desired.
-O|--crop-thumbnails <size> : After resizing, crop thumbnails to <size>
                              (given in ImageMagick format). Specify
                              like -c '' to prevent cropping.
                              [$CROP_THUMBNAILS]
-p|--photo-template <path>  : Use <path> as HTML template for photos.
-P|--print-photo-template   : Prints out the internal photo template
                              and exits.
-q|--quality <q>            : Compression quality for photos. [$QUALITY]
-Q|--thumbnail-quality <q>  : Compression quality for thumbnails. [$THUMBNAIL_QUALITY]
-r|--recurse                : Recursively add the selected albums'
                              sub-albums
-R|--min-rating <rating>    : Only include photos with a minimum rating
                              of <rating>. [$MIN_RATING]
-s|--sort-photos-by <field> : Sort photos in albums by database column
                              <field>. [$SORT_PHOTOS_BY]
-S|--sort-albums-by <field> : Sort each album's sub-albums by database
                              column <field>. [$SORT_ALBUMS_BY]
-t|--target-dir <dir>       : Gallery output directory. WARNING: Will
                              get completely purged, unless -n or -N is
                              specified! [$TARGET_DIR]
-T|--date-format <format>   : Format used for displaying album creation
                              dates. [$DATE_FORMAT]
-u|--use-srgb-profile       : Convert images to sRGB color space (if
                              not in sRGB already). Recommended for
                              images presented in The Web.
-U|--srgb-profile-path      : Where to find the sRGB ICC profile file.
                              [$SRGB_PROFILE_PATH]
-v|--verbose                : Talk a lot.
-V|--version                : Print version number and exit.
-w|--win-autorun <label>    : Create Windows-style AUTORUN.INF style
                              in target directory. Effect: use <label>
                              as drive description and open gallery
                              start page in browser.
-x|--exclude-photos <patt>  : Explicitly exclude the specified photo(s)
                              from the gallery. May be used multiple
                              times.
-X|--exclude-albums <patt>  : Explicitly exclude the specified album(s)
                              from the gallery. May be used multiple
                              times. Useful in conjunction with -r.
-y|--stylesheet <path>      : Use <path> as css stylesheet for album
                              and photo pages.
-Y|--print-stylesheet       : Prints out the internal stylesheet and
                              exits.
An internal default will be used for album template, photo template
and/or stylesheet, if their corresponding options (-a, -p and -y,
respectively) are not given.
Albums pattern always refer to full albums URL; however, SQL wildcards
may be used. Hence '%foo' selects both albums '/foo' and '/bar/foo',
while '%foo%' additionally selects '/foo/bar' and '/bar/foo/blah'.
Album exclusion patterns (-X) work in the same way; photo exlusion
patterns, on the other hand, refer to the photos' file names only.
Please note that literal percent characters (%) and underscores (_)
need to be escaped using '\\'.
This programm relies on the Perl library SysUtils.pm. Furthermore,
digikam2web requires imagemagick and the sqlite3 command-line client
to be installed and in PATH. If you want to include video files into
your galleries, you'll also need ffmpeg.
EOT
}


#--- Album information retrieval and album tree creation --


#--- Retrieve album information from digikam.db compliant with
#    (optionally given) conditions as well as include
#    and exclude SQL patterns for album names and collections:
#
#    arrayref $albums getAlbums(
#                       [hashref $column_names2values]
#                       [arrayref $incl
#                       [, arrayref $excl
#                       [, arrayref $inclCols
#                       [, arrayref $exclCols]]]]
#                     )
#
sub getAlbums {
  # Extract conditions as well as include and exclude pattern
  # from arguments
  my $cond = {};
  my $incl;
  my $excl;
  my $inclCols;
  my $exclCols;
  while (@_) {
    my $arg = shift;
    if (ref($arg) eq 'HASH') {
      $cond = $arg;
    }
    elsif (ref($arg) eq 'ARRAY') {
      if (defined($inclCols)) {
        $exclCols = $arg;
      }
      elsif (defined($excl)) {
        $inclCols = $arg;
      }
      elsif (defined($incl)) {
        $excl = $arg;
      }
      else {
        $incl = $arg;
      }
    }
  }

  my $albumTable; # Table(s) containing album informatino
  my $pathCols;   # Column(s) containing paths to albums' directories
  if ($DB_VERSION == 3) {
    $albumTable = 'Albums';
    $pathCols ='Albums.url';
  }
  else {
    $albumTable = 'Albums INNER JOIN AlbumRoots ON Albums.albumRoot = AlbumRoots.id';
    $pathCols = '(AlbumRoots.specificPath || Albums.relativePath)';
  }

  # Build WHERE clause
  my $addCond = undef;
  # - for including albums with matching urls
  if ($incl && scalar(@{$incl}) > 0) {
    my @urls = map {$_ =~ m/^\// ? $_ : "/$_"} (@{$incl});
    my @conds = map {"$pathCols LIKE '" . &sqlEscape(&SysUtils::qqUnescape($_)) . "' ESCAPE '\\\'"} (@urls);
    $addCond = '(' . join(' OR ', @conds) . ')';
  }
  # - for excluding albums with matching urls
  if ($excl && scalar(@{$excl}) > 0) {
    my @urls = map {$_ =~ m/^\// ? $_ : "/$_"} (@{$excl});
    my @conds = map {"$pathCols NOT LIKE '" . &sqlEscape(&SysUtils::qqUnescape($_)) . "' ESCAPE '\\'"} (@urls);
    if ($addCond) {
      $addCond .= ' AND ';
    }
    else {
      $addCond = '';
    }
    $addCond .= join(' AND ', @conds);
  }
  # - for including albums with matching collections
  if ($inclCols && scalar(@{$inclCols}) > 0) {
    my @cols = @{$inclCols};
    if (grep(/^$/, @cols)) {
      push(@cols, 'Uncategorized Album')
    }
    my @conds = map {"Albums.collection LIKE '" . &sqlEscape(&SysUtils::qqUnescape($_)) . "' ESCAPE '\\\'"} (@cols);
    if ($addCond) {
      $addCond .= ' AND ';
    }
    else {
      $addCond = '';
    }
    $addCond .= '(' . join(' OR ', @conds) . ')';
  }
  # - for excluding albums with matching collections
  if ($exclCols && scalar(@{$exclCols}) > 0) {
    my @cols = @{$exclCols};
    if (grep(/^$/, @cols)) {
      push(@cols, 'Uncategorized Album')
    }
    my @conds = map {"Albums.collection NOT LIKE '" . &sqlEscape(&SysUtils::qqUnescape($_)) . "' ESCAPE '\\'"} (@cols);
    if ($addCond) {
      $addCond .= ' AND ';
    }
    else {
      $addCond = '';
    }
    $addCond .= join(' AND ', @conds);
  }
  if ($addCond and (scalar(keys(%{$cond})) > 0)) {
    $addCond = " AND ($addCond)";
  }
  # Retrieve information on selected albums
  my $albums = &getRecords($albumTable, $cond, $addCond);
  # Enrich album entries with some additional information
  for (my $i = 0; $i < scalar(@{$albums}); $i++) {
    my $album = $albums->[$i];
    if ($album->{'Albums.id'}) {
      $album->{id} = $album->{'Albums.id'};
    }

    # For db version 4...
    if ($DB_VERSION == 4) {
      # Build url from specific and relative path
      $album->{url} = $album->{'specificPath'} . $album->{'relativePath'};
    }

    my $path = $album->{url};
    $path =~ s/\//$SysUtils::DIRSEP/g;

    # For db version 3...
    if ($DB_VERSION == 3) {
      # Build full path from (single!) album root and
      # relative path provided by database
      $path = "$DIGIKAM_ROOT$path";
    }

    $album->{date} =~ s/T/, /g;
    $album->{path} = $path;
    $album->{url_regex} = &SysUtils::regexEscape($album->{url});
    $album->{parent} = undef;
    $album->{children} = [];
    unless ($album->{caption}) {
      my $caption = $album->{url};
      if ($caption =~ m/.*\/([^\/]+?)$/) {
        $caption = $1;
      }
      $album->{caption} = $caption;
    }
  }
  return $albums;
}

#--- Determine the given albums' parent-child relationship
#
#    void makeTrees(arrayref $albums)
#
sub makeTrees {
  my $albums = shift;
  # Pairwise compare all albums
  foreach my $a1 (@{$albums}) {
    foreach my $a2 (@{$albums}) {
      unless ($a1 == $a2) {
        my $regex = $a2->{url_regex};
        # If albums 1's url is longer than album 2's url
        # by a slash, followed by an arbitrary number of
        # non-slash characters, a1 must be a direct child
        # of a2
        if ($a1->{url} =~ m/^$regex\/[^\/]+$/) {
          $a1->{parent} = $a2;
          push(@{$a2->{children}}, $a1);
        }
      }
    }
  }
}

#--- Create a common root for an arbitrary number of
#    album sub-trees
#
#    hashref $root makeCommonRoot(arrayref $albums)
#
sub makeCommonRoot {
  my $albums = shift;
  # Create a common (still emtpy) root node
  my $root = &makeEmptyRoot();
  foreach my $album (@{$albums}) {
    # Add parent-less albums (which are the roots of
    # the subtrees) to the common root
    unless (defined($album->{parent})) {
      $album->{parent} = $root;
      push(@{$root->{children}}, $album);
    }
  }
  # If the new common root does have only one child,
  # use this child as common root
  if (scalar(@{$root->{children}}) == 1) {
    $root = $root->{children}->[0];
    $root->{parent} = undef;
  }
  return $root;
}

#--- Build a hash with each album's $key as key
#
#    hashref $index getAlbumIndex(arrayref $albums, string $key)
#
sub getAlbumIndex {
  my $albums = shift;
  my $key = shift;
  my $index = {};
  foreach my $album (@{$albums}) {
    $index->{$album->{$key}} = $album
  }
  return $index;
}

#--- For all given albums, recursively build their sub-album trees.
#    Requires access to the tree of all albums.
#
#    void addSubAlbums(arrayref $albums, hashref $allRoot)
#
sub addSubAlbums {

  #--- Traverses the whole album tree and starts including
  #    albums into the $albums list once an initially
  #    selected album (identified by its url) is reached
  #
  sub expandSubRoot {
    # arraryef eventually containing selected albums plus
    # all their subalbums
    my $albums = shift;
    # Index knowing by URL which albums have initially been
    # selected
    my $selectedByUrlIndex = shift;
    # Current position in tree of all albums
    my $currentRoot = shift;
    # Whether or not to include this album and its sub-albums
    my $include = shift;
    # If current album has also initially been selected...
    if (exists($selectedByUrlIndex->{$currentRoot->{url}})) {
      # ...include it, as well as its sub-albums
      $include = 1;
    }
    # If current album is to be included...
    if ($include) {
      # ...clone it...
      my $album = &makeClone($currentRoot);
      # ...make it forget its original parent...
      $album->{parent} = undef;
      # ...see whether it should be excluded...
      my $matches = &getAlbums({'Albums.id' => $album->{id}}, [],
                               \@EXCLUDE_ALBUMS,
                               \@INCLUDE_COLLECTIONS,
                               \@EXCLUDE_COLLECTIONS);
      if (scalar(@{$matches}) > 0) {
        # ...and store it in the final list of selected albums
        # if it is not to be exluded
        push(@{$albums}, $album);
      }
    }
    # Do the same for all the current album's children
    foreach my $child (@{$currentRoot->{children}}) {
      &expandSubRoot($albums, $selectedByUrlIndex, $child, $include);
    }
  }

  #--- addSubAlbums main block
  my $albums = shift;
  my $allRoot = shift;

  # Build url-based index of all selected albums
  my $selectedByUrlIndex = &getAlbumIndex($albums, 'url');
  # Empty the list of selected albums
  @{$albums} = ();
  # Build sub-trees
  &expandSubRoot($albums, $selectedByUrlIndex, $allRoot, 0);

}

#--- Make all selected albums have one common root album
#
#    hashref $root makeFlatRoot(arrayref $albums)
#
sub makeFlatRoot {
  my $albums = shift;
  # Create a new and empty root album
  my $root = &makeEmptyRoot();
  # For each album...
  foreach my $album (@{$albums}) {
    # ...make the new root its parent
    $album->{parent} = $root;
    push(@{$root->{children}}, $album);
    $album->{children} = [];
  }
  # If there's only one sub-album, use it as root album
  # (instead of the new root)
  if (scalar(@{$root->{children}}) == 1) {
    $root = $root->{children}->[0];
    $root->{parent} = undef;
  }
  return $root;
}

#--- Returns a hashref containing all keys an album
#    record should have
#
#    hashref $root makeEmptyRoot
#
#    !!! Note: Definitely not platform-independent !!!
#
sub makeEmptyRoot {
  # Determine current time
  (my $now, my $exit) = &SysUtils::execCommand("date +$DATE_FORMAT");
  chomp($now);
  # Create hashref with all keys initially expected
  # to be present for an album
  my $root = {
    caption => $HOME_STRING,
    children => [],
    date => $now,
    id => -1,
    name => $HOME_STRING,
    parent => undef,
    path => "$DIGIKAM_ROOT$SysUtils::DIRSEP",
    photos => [],
    url => '/',
    url_regex => '\\/'
  };
  return $root;
}

#--- Clones a hashref with regard to its keys: arrayrefs and
#    hashrefs are cloned as empty structures, and scalar values
#    are only copied if not requested to return an empty clone
#
#    hashref $clone makeClone(hashref $ref [, boolean $empty])
#
sub makeClone {
  my $template = shift;
  # Default: Don't copy scalar values
  my $empty = 0;
  if (@_) {
    $empty = shift;
  }
  my $clone = {};
  # Iterate through the template's keys
  foreach my $key (keys(%{$template})) {
    # If value is an arrayref, assign empty array
    if (ref($template->{$key}) eq 'ARRAY') {
      $clone->{$key} = [];
    }
    # If value is a hashref, assign empty hash
    elsif (ref($template->{$key}) eq 'HASH') {
      $clone->{$key} = {};
    }
    # Value is a scalar; copy, unless empty is TRUE
    else {
      $clone->{$key} = ($empty ? undef : $template->{$key});
    }
  }
  return $clone;
}


sub removeEmptyAlbums {

  sub doRemoveIfEmpty {
    my $album = shift;
    foreach my $child (@{$album->{children}}) {
      &doRemoveIfEmpty($child);
    }
    if (scalar(@{$album->{photos}}) == 0) {
      my $parent = $album->{parent};
      my @newChildren = ();
      foreach my $child (@{$parent->{children}}) {
        unless ($child == $album) {
          push(@newChildren, $child);
        }
      }
      foreach my $child (@{$album->{children}}) {
        $child->{parent} = $parent;
        push(@newChildren, $child);
      }
      $parent->{children} = \@newChildren;
    }
  }

  my $root = shift;
  foreach my $child (@{$root->{children}}) {
    &doRemoveIfEmpty($child)
  }
}



#--- Get ids of tags
#
#
#
sub getTagIds {
  my %inclSet = ();
  my %exclSet = ();
  if (@INCLUDE_TAGS) {
    my @conds = map {"name LIKE '" . &sqlEscape(&SysUtils::qqUnescape($_)) . "' ESCAPE '\\\'"} (@INCLUDE_TAGS);
    my $conds = join(' OR ', @conds);
    my $tags = &getRecords('Tags', ['id'], $conds);
    foreach my $tag (@{$tags}) {
      my $id = $tag->{id};
      $inclSet{$id} = $id;
      $tags = &getRecords('TagsTree', ['id'], {pid => $id});
      foreach my $record (@{$tags}) {
        $inclSet{$record->{id}} = $record->{id};
      }
    }
  }
  if (@EXCLUDE_TAGS) {
    my @conds = map {"name LIKE '" . &sqlEscape(&SysUtils::qqUnescape($_)) . "' ESCAPE '\\\'"} (@EXCLUDE_TAGS);
    my $conds = join(' OR ', @conds);
    my $tags = &getRecords('Tags', ['id'], $conds);
    foreach my $tag (@{$tags}) {
      my $id = $tag->{id};
      $exclSet{$id} = $id;
      delete($inclSet{$id});
      $tags = &getRecords('TagsTree', ['id'], {pid => $id});
      foreach my $record (@{$tags}) {
        $exclSet{$record->{id}} = $record->{id};
        delete($exclSet{$record->{id}});
      }
    }
  }
  @INCLUDE_TAG_IDS = keys(%inclSet);
  @EXCLUDE_TAG_IDS = keys(%exclSet);
}


#--- Add photo information to all selected albums; requires
#    both the tree and the list of selected albums
#
#    void addPhotos(hashref $root, arrayref $albums)
#
sub addPhotos {
  my $root = shift;
  my $albums = shift;

  # What files to look for
  my $validExtRegex = '(' . join('|', @VALID_EXTENSIONS) . ')';

  # Iterate through all selected albums
  foreach my $album (@{$albums}) {
    # List of album's photos
    $album->{photos} = &getPhotos($album->{id});
    # Iterate through the currently processed album's photos
    foreach my $photo (@{$album->{photos}}) {
      my $name = $photo->{name};
      # If photo is of valid file type...
      if ($name =~ m/\.($validExtRegex)$/gi) {
        # Build up a photo_id => photo index
        $ALL_PHOTOS_INDEX->{$photo->{id}} = $photo;
        # Clarify the photo's album memebership
        if ($FLATTEN_PHOTOS) {
          push (@{$root->{photos}}, $photo);
          $photo->{album} = $root;
        }
        else {
          $photo->{album} = $album;
        }
      }
    }
  }

  # If photos have been flattened...
  if ($FLATTEN_PHOTOS) {
    # re-sorting is required
    if ($SORT_NUMERIC{$SORT_PHOTOS_BY}) {
      @{$root->{photos}} = sort {$a->{$SORT_PHOTOS_BY} <=> $b->{$SORT_PHOTOS_BY}} (@{$root->{photos}});
    }
    else {
      @{$root->{photos}} = sort {lc($a->{$SORT_PHOTOS_BY}) cmp lc($b->{$SORT_PHOTOS_BY})} (@{$root->{photos}});
    }
  }

}

#--- Retrieves information on all photos (with a minimum rating
#    and certain include/exclude tags, if specified) contained
#    in the album witht the given id
#
#    arrayref $photos getPhotos(int $albumId)
#
sub getPhotos {
  my $albumId = shift;
  # Restrain to valid file types
  my @conds = map {"name LIKE '%." . &sqlEscape($_) . "'"} (@VALID_EXTENSIONS);
  my $conds = 'AND (' . join(' OR ', @conds) . ')';
  # Consider photo exclusion conditions, if specified
  if (scalar(@EXCLUDE_PHOTOS) > 0) {
    @conds = map {"name NOT LIKE '" . &sqlEscape(&SysUtils::qqUnescape($_)) . "' ESCAPE '\\'"} (@EXCLUDE_PHOTOS);
    $conds .= ' AND (' . join(' AND ', @conds) . ')';
  }

  # Consider image tags, if specified
  if (scalar(@INCLUDE_TAG_IDS) > 0) {
    @conds = map {"tagid = $_"} @INCLUDE_TAG_IDS;
    $conds .= ' AND Images.id IN (SELECT imageid FROM ImageTags WHERE ' . join(' OR ', @conds) . ')';
  }
  if (scalar(@EXCLUDE_TAG_IDS) > 0) {
    @conds = map {"tagid = $_"} @EXCLUDE_TAG_IDS;
    $conds .= ' AND Images.id NOT IN (SELECT imageid FROM ImageTags WHERE ' . join(' OR ', @conds) . ')';
  }


  my $tableNames; # Table(s) containing image information
  my $albumIdCol; # Column containing the images' albums' ids
  if ($DB_VERSION == 3) {
    $tableNames = 'Images LEFT OUTER JOIN ImageProperties ON Images.id = ImageProperties.imageid';
    $albumIdCol = 'Images.dirid';
    # Consider rating, if specified
    if ($MIN_RATING > 0) {
      $conds .= " AND ImageProperties.property = 'Rating' AND ImageProperties.value >= $MIN_RATING";
    }
  }
  else {
    $tableNames = 'Images LEFT OUTER JOIN ImageInformation ON Images.id = ImageInformation.imageid';
    $albumIdCol = 'Images.album';
    # Consider rating, if specified
    if ($MIN_RATING > 0) {
      $conds .= " AND ImageInformation.rating >= $MIN_RATING";
    }
  }
  my $photos = &getRecords($tableNames, {$albumIdCol => $albumId}, $conds);

  # For db version 4...
  if ($DB_VERSION == 4) {
    # For each photo...
    foreach my $photo (@{$photos}) {
      # Copy some fields to equivalent db version 3 fields,
      # which are still required by digikam2web
      $photo->{dirid} = $photo->{album};
      $photo->{datetime} = $photo->{creationDate};
      # Retrieve all the image's comments...
      my $comments = getRecords('ImageComments', ['comment'], {imageid => $photo->{id}});
      # ...and join and store them in the version 3 caption field
      $photo->{caption} = join("\n", map {$_->{comment}} (@{$comments}));
    }
  }
  return $photos;
}

#--- Sort an album's photos according to specified criterion;
#    recursively, sort photos in the album's sub-albums
#
#    void sortPhotos(hashref $album)
#
sub sortPhotos {
  my $album = shift;
  # Figure out whether to sort numerically or lexically
  if ($SORT_NUMERIC{$SORT_PHOTOS_BY}) {
    @{$album->{photos}} = sort {$a->{$SORT_PHOTOS_BY} <=> $b->{$SORT_PHOTOS_BY}} (@{$album->{photos}});
  }
  else {
    @{$album->{photos}} = sort {lc($a->{$SORT_PHOTOS_BY}) cmp lc($b->{$SORT_PHOTOS_BY})} (@{$album->{photos}});
  }
  # Assign integers 1..<no of photos> to the sorted photos
  for (my $i = 0; $i < scalar(@{$album->{photos}}); $i++) {
    $album->{photos}->[$i]->{n} = $i + 1;
  }
  foreach my $child (@{$album->{children}}) {
    &sortPhotos($child);
  }
}


#--- Page creation ----------------------------------------


#--- To all albums, recursively add information required
#    for building relative URLs etc.
#
#    void addWebInfo(hashref $album, int $level)
#
#    Level should be 0 when called on root album
#
sub addAlbumWebInfo {
  my $album = shift;
  my $level = shift;

  $album->{level} = $level;


  # Unlees we're dealing with the root album (/) ...
  if ($album->{url} =~ m/.*\/([^\/]+)/) {
    # ...take last segment of url as album name
    $album->{name} = $1;
  }
  else {
    # Otherwise, take the album's caption as name
    $album->{name} = $album->{caption};
  }
  # Create unique album name by prepending the album's id
  $album->{url_name} = $album->{id} . '-' . $album->{name};

  my $rel_url;
  my $rel_path;
  # Create relative url/path of parent album directory
  if ($album->{parent}) {
    $rel_url = $album->{parent}->{rel_url} . '/' . $album->{url_name};
    $rel_path = $album->{parent}->{rel_path} . $SysUtils::DIRSEP . $album->{url_name};
  }
  else {
    $rel_url = '.';
    $rel_path = '.';
  }

  $album->{rel_url} = $rel_url;
  $album->{rel_path} = $rel_path;

  # Create relative url of root album's directory
  $album->{base_url} = '../' x $level;

  # Number of photos in this album and (recursively) its sub-albums
  my $totalPhotos = scalar(@{$album->{photos}});
  # Recurseively add web info to the album's children
  foreach my $child (@{$album->{children}}) {
    &addAlbumWebInfo($child, $level + 1);
    $totalPhotos += $child->{total_photos};
  }
  $album->{total_photos} = $totalPhotos;

  # Sort sub-albums according to specified criterion
  if ($SORT_NUMERIC{$SORT_ALBUMS_BY}) {
    @{$album->{children}} = sort {$a->{$SORT_ALBUMS_BY} <=> $b->{$SORT_ALBUMS_BY}} (@{$album->{children}});
  }
  else {
    @{$album->{children}} = sort {lc($a->{$SORT_ALBUMS_BY}) cmp lc($b->{$SORT_ALBUMS_BY})} (@{$album->{children}});
  }
}

#
sub getAlbumsInTree {
  my $root = shift;

  my @albums = ();
  my @queue = ($root);
  while (scalar(@queue) > 0) {
    my $album = shift(@queue);
    push(@albums, $album);
    push(@queue, @{$album->{children}});
  }

  return \@albums;
}


#--- To all photos within the album tree, add information
#    required for building relative URLs etc.
#
#    void addPhotoWebInfo(hashref $root, arraref $albums)
#
#    Level should be 0 when called on root album
#
sub addPhotoWebInfo {
  my $root = shift;
  my $albums = shift;

  my $movieExtRegex = '(' . join('|', @MOVIE_EXTENSIONS) . ')';

  # Iterate through all selected albums
  foreach my $album (@{$albums}) {
    my $dir;
    # Determine photo page's directory: If photo flattening
    # has been requested...
    if ($FLATTEN_PHOTOS) {
      # ...all photos show up in the root album...
      $dir = "$TARGET_DIR$SysUtils::DIRSEP" . $root->{rel_path};
    }
    else {
      # ...and in their own album otherwise
      $dir = "$TARGET_DIR$SysUtils::DIRSEP" . $album->{rel_path};
    }
    # Iterate through the currently processed album's photos
    foreach my $photo (@{$album->{photos}}) {
      my $name = $photo->{name};
      # The photo filename's extension
      my $ext = &SysUtils::getExt($name);
      # Cleanup SQLite date/time format
      $photo->{datetime} =~ s/T/, /g;
      # Filename without extension
      $photo->{base_name} = &SysUtils::getName($name);
      # Ensure unique filename in gallery by prepending
      # the photo's id to its filename (w/o extension)
      $photo->{url_name} = $photo->{id} . '-' . $photo->{base_name};
      # Unique filename for the final jpeg image file in the gallery
      $photo->{photo_name} = $photo->{url_name} . '.jpg';
      # Same for the original photo file (in case inclusion
      # of original files has been requested)
      $photo->{orig_name} = $photo->{id} . '-' . $photo->{base_name} . ".$ext";
      # Path to original photo file in digikam directory
      $photo->{source_path} = $album->{path} . $SysUtils::DIRSEP . $name;
      # Where to finally store the processed photo...
      $photo->{photo_path} = "$dir${SysUtils::DIRSEP}photos$SysUtils::DIRSEP" . $photo->{photo_name};
      # ...its thumbnail preview...
      $photo->{thumb_path} = "$dir${SysUtils::DIRSEP}thumbs$SysUtils::DIRSEP" . $photo->{photo_name};
      # ...and a copy of its original file
      $photo->{orig_path} = "$dir${SysUtils::DIRSEP}origs$SysUtils::DIRSEP" . $photo->{orig_name};
      # Find out whether current photo is actually a movie
      if ($name =~ m/\.$movieExtRegex$/i) {
        $photo->{is_movie} = 1;
      }
      # Build up a photo_id => photo index
      $ALL_PHOTOS_INDEX->{$photo->{id}} = $photo;
      # Clarify the photo's album memebership
      if ($FLATTEN_PHOTOS) {
        push (@{$root->{photos}}, $photo);
        $photo->{album} = $root;
      }
      else {
        $photo->{album} = $album;
      }
    }
  }

  # If photos have been flattened...
  unless ($FLATTEN_PHOTOS) {
    # Set up album thumbnails
    foreach my $album (@{$albums}) {
      # Path to current album's files
      my $dir = "$TARGET_DIR$SysUtils::DIRSEP" . $album->{rel_path};
      my $thumb = undef;
      # If album already has an icon assigned...
      if ($album->{icon}) {
        # ...get the corresponding photo's data
        $thumb = &getRecords('Images', {id => $album->{icon}})->[0];
        # If we have seen this photo before...
        if (exists($ALL_PHOTOS_INDEX->{$thumb->{id}})) {
          # ...just take it (and its thumbnail)
          $thumb = &makeClone($ALL_PHOTOS_INDEX->{$thumb->{id}});
        }
        else {
          # Otherwise, set up everything required for
          # thumbnail creation when building album pages

          if ($DB_VERSION == 4) {
            $thumb->{dirid} = $thumb->{album};
          }
          my $thumbAlbum = $ALL_ALBUMS_INDEX->{$thumb->{dirid}};
          $thumb->{base_name} = &SysUtils::getName($thumb->{name});
          $thumb->{url_name} = $thumb->{id} . '-' . $thumb->{base_name};
          $thumb->{photo_name} = $thumb->{url_name} . '.jpg';
          $thumb->{source_path} = $thumbAlbum->{path} . $SysUtils::DIRSEP . $thumb->{name};
        }
      }
      # If no album icon has been assigned...
      else {
        # Just take the album's first photo
        if (scalar(@{$album->{photos}}) > 0) {
          $thumb = &makeClone($album->{photos}->[0]);
        }
        else {
        # --- What do do?
        }
      }
      # Finally (re-)assign thumbnail to album
      if (defined($thumb)) {
        $album->{thumb} = $thumb;
        $thumb->{thumb_path} = "$dir${SysUtils::DIRSEP}thumb.jpg";
        if ($thumb->{name} =~ m/\.$movieExtRegex$/i) {
          $thumb->{is_movie} = 1;
        }
      }
    }
  }
}

#--- Build the gallerie's (sub-)directory structure and write
#    the given stylesheet content into the 'styles.css' file
#    within the gallerie's root directory
#
#    void prepareTargetDir(string $stylesheet)
#
sub prepareTargetDir {

  #--- Little helpers

  sub rmFile {
    my $path = shift;
    unlink($path);
  }

  sub rmDir {
    my $path = shift;
    rmdir($path);
  }

  #--- prepareTargetDir main block

  my $stylesheet = shift;
  if (-e $TARGET_DIR) {
    # Clean target dir unless existing directories and files
    # are to be partially re-used
    unless ($DONT_RECREATE or $ONLY_NEW_PHOTOS) {
      &SysUtils::visitFiles($TARGET_DIR, \&rmFile, '*', '*', \&rmDir, $SysUtils::VISIT_FILES_BEFORE_DIRS | $SysUtils::VISIT_DIRS_BEFORE_PROCESSING);
    }
  }
  else {
    &SysUtils::makePath($TARGET_DIR);
  }
  # Write the stylesheet
  &SysUtils::writeToFile("$TARGET_DIR${SysUtils::DIRSEP}styles.css", $stylesheet);
}

#--- From the given string(ref), extract all <@tag>...</@tag>
#    pairs (for @link tags, unless specified otherwise) and
#    return a hashref containing marker => <@tag>...<@/tag> mappings
#
#    hashref $mapping parseLinks(stringref $strRef [, $tag])
#
sub parseLinks {
  my $strRef = shift;
  my $tag = 'link';
  if (@_) {
    $tag = shift;
  }
  my $mapping = {};
  my %links = &SysUtils::extractMatches($strRef, "<\@${tag}[^>]*>.+?</\@$tag>");
  foreach my $marker (keys(%links)) {
    $mapping->{$marker} = $links{$marker};
  }
  return $mapping;
}

#--- From the given string(ref), extract the *text between* all
#    <@tag>[some text]</@tag> pairs and return a hashref containing
#    marker => [some text] mappings
#
#    hashref $tags parseSections(stringref $strRef, string $tag)
#
sub parseSections {
  my $strRef = shift;
  my $tag = shift;

  my %tags = &SysUtils::extractMatches($strRef, "<\@${tag}.*?>.*?</\@$tag>");
  foreach my $marker (keys(%tags)) {
    $tags{$marker} =~ s/<\/?\@$tag>//g;
  }
  return \%tags;
}

#--- From the given string(ref), extract the *text between* *one*
#    <@tag>...</@tag> pair and return a hashref containing the
#    extracted text's marker (key 'marker') and the text
#    itself (key 'section'). If more than one matching tag pair
#    is found, the program dies with an error message.
#
#    hashref $result parseSingleSection(stringref $strRef [, string $tag])
#
sub parseSingleSection {
  my $strRef = shift;
  my $tag = shift;

  my $result = {};
  my %tags = &SysUtils::extractMatches($strRef, "<\@${tag}.*?>.*?</\@$tag>");
  my @markers = keys(%tags);
  if (scalar(@markers) > 1) {
    die("More than one <\@$tag>...</\@$tag> section found!");
  }
  else {
    $result->{marker} = $markers[0];
    $tags{$markers[0]} =~ s/<\/?\@$tag>//g;
    $result->{section} = $tags{$markers[0]}
  }
  return $result
}

#--- Replace in the given string(ref) all markers specified in
#    the given mapping with either 1) the given hyperlink or 2)
#    the original text (if no href is given).
#
#    void reinsertLinks(stringref $strRef, hashref $mapping,
#                       string $href [, string $tag])
#
sub reinsertLinks {
  my $strRef = shift;
  my $mapping = shift;
  my $href = shift;
  my $tag = 'link';
  if (@_) {
    $tag = shift;
  }
  foreach my $marker (keys(%{$mapping})) {
    my $link = $mapping->{$marker};
    if ($href) {
      $link =~ s/<\@$tag/<a href="$href"/g;
      $link =~ s/<\/\@$tag>/<\/a>/g;
    }
    else {
      $link =~ s/<\/?\@$tag>//g;
    }
    &SysUtils::reinsertAtMarker($strRef, $link, $marker);
  }
}

#--- Uses the above methods to extract all special tag pairs
#    from the photo page tempate and returns a hashref containing
#    further hashrefs which contain the actual parsing results:
#    - 'navprev', 'navup', 'navnext' => Descriptions of
#      all identified navigation elements to the previous and
#      the next photo as well as back to the photo's album.
#      Contents see below (parsePhotoNav method).
#    - 'origs' => Describes all identified links to the photo's
#      original file
#    - 'page' => The parsed template string which now
#      contains markers instead of the original tag pairs
#
#      hashref $result parsePhototemplate(string $page)
#
sub parsePhotoTemplate {

  #--- Extracts from the given stringref all navigation tag pairs of
  #    the given type ($suffix = 'up' => parse <@navup>...</@navup>
  #    ect. ) and returns a hashref describing the parsing results,
  #    one hashref per link, using the inserted markers as keys. The
  #    actual link descriptions provide the following information:
  #    - 'section' =>  What's left of the navigation section after
  #      extracting all links.
  #    - 'links' => The result of parsing the given navigation
  #      element for <@link>...<@/link> sections. For details, see
  #      above (parseLinks method).
  #    - 'nolink' => Corresponding information about <@nolink>...</@nolink>
  #      sections.
  #
  #    hashref $result parsePhotoNav(stringref $pageRef, string $suffix)
  #
  sub parsePhotoNav {
    my $pageRef = shift;
    my $suffix = shift;

    my $result = {};

    my $navs = &parseSections($pageRef, "nav$suffix");
    foreach my $marker (keys(%{$navs})) {
      my $nav = $navs->{$marker};
      my $links = &parseLinks(\$nav, 'link');
      my $nolinks = &parseLinks(\$nav, 'nolink');
      $result->{$marker} = {section => $nav, links => $links, nolinks => $nolinks};
    }
    return $result;
  }

  #--- parsePhotoTemplate main block

  my $page = shift;

  my $result = {};
  $result->{navprev} = &parsePhotoNav(\$page, 'prev');
  $result->{navup} = &parsePhotoNav(\$page, 'up');
  $result->{navnext} = &parsePhotoNav(\$page, 'next');
  $result->{origs} = &parseLinks(\$page, 'orig');
  $result->{page} = $page;
  return $result;
}

#--- Create html files for viewing individual photos within the
#    given album (and its sub-albums), using the given template
#
#    void makePhotoPages(hashref $album, stringref $template)
#
sub makePhotoPages {
  my $album = shift;
  my $template = shift;

  # Ensure the target directories exists (may not be the case
  # if the album structre has changed and the -n or -N option
  # have been given)
  my $dir = "$TARGET_DIR$SysUtils::DIRSEP" . $album->{rel_path};
  &SysUtils::makePath($dir);
  mkdir("$dir${SysUtils::DIRSEP}photos");
  mkdir("$dir${SysUtils::DIRSEP}thumbs");
  if ($INCLUDE_ORIGS or $INCLUDE_MOVIE_ORIGS) {
    mkdir("$dir${SysUtils::DIRSEP}origs");
  }

  # Iterate through all the album's photos
  for (my $i = 0; $i < scalar(@{$album->{photos}}); $i++) {
    my $photo = $album->{photos}->[$i];
    # Build the html file's path
    my $photoPath = "$dir${SysUtils::DIRSEP}photos${SysUtils::DIRSEP}" . $photo->{url_name} . '.html';
    # Obtain a copy of the parsed template string
    my $page = $template->{page};
    # Fill in the curren photo's data into the corresponding
    # previously marked sections
    &insertOrigLinks($photo, \$page, $template);
    &insertPhotoNavigation($album, $i, \$page, $template);
    &insertObjectInfo($photo, \$page);
    &insertGeneralInfo($album, \$page, $photo);
    &processEvalTags($album, \$page, $photo);
    # Finally!
    &SysUtils::writeToFile($photoPath, $page);
  }
  # Recursively build html pages for this album's sub-ablums'
  # photos
  foreach my $child (@{$album->{children}}) {
    &makePhotoPages($child, $template);
  }
}

#--- Based on the given $photo, reinsert into the html $page(ref)
#    links to the photo's original file as described in the.
#    parsed template.
#
#    void insertOriginalLinks(hashref $photo,
#                             stringref $pageRef,
#                             hashref $template)
#
sub insertOrigLinks {
  my $photo = shift;
  my $pageRef = shift;
  my $template = shift;

  my $href;
  # Only include links to original files if requested by user
  if ($INCLUDE_ORIGS or ($photo->{is_movie} and $INCLUDE_MOVIE_ORIGS)) {
    $href='../origs/' . $photo->{orig_name};
  }
  &reinsertLinks($pageRef, $template->{origs}, $href, 'orig');
}

#--- Reinsert into the given html page(ref), which is the given
#    photo page of the given album's ith photo, all navigation
#    links.
#
#    void insertPhotoNavigation(hashref $album,
#                               int $i,
#                               stringref $pageRef,
#                               hashref $template,
#
sub insertPhotoNavigation {

  #--- Re-inserts the given hypelink into the given html
  #    page(ref) at markers described by the parsed navigation
  #    element (either prev, next or up)
  #
  #    void insertPhotoNav(stringref $pageRef,
  #                        hashref $navTemplate,
  #                        string $href)
  sub insertPhotoNav {
    my $pageRef = shift;
    my $navTemplate = shift;
    my $href = shift;

    # Iterate through all occurences of prev, next or up, resp.
    foreach my $marker (keys(%{$navTemplate})) {
      my $nav = $navTemplate->{$marker}->{section};
      # If a hyperlink is given...
      if ($href) {
        # ...insert it at the current marker position...
        &reinsertLinks(\$nav, $navTemplate->{$marker}->{links}, $href);
      }
      else {
        # ...or otherwise delete the whole section
        my $links = $navTemplate->{$marker}->{links};
        foreach my $lmarker (keys(%{$links})) {
          &SysUtils::reinsertAtMarker(\$nav, '', $lmarker);
        }
      }
      # Process the corresponding nolink sections
      my $nolinks = $navTemplate->{$marker}->{nolinks};
      foreach my $nlmarker (keys(%{$nolinks})) {
        my $nolink = $navTemplate->{$marker}->{nolinks}->{$nlmarker};
        # If a hyperlink is given...
        if ($href) {
          # ...completely remove the nolink section...
          $nolink =~ s/<\@nolink>.*?<\/\@nolink>//g;
        }
        else {
          # ...or replace it with its contents otherwise
          $nolink =~ s/<\/?\@nolink>//g;
        }
        &SysUtils::reinsertAtMarker(\$nav, $nolink, $nlmarker);
      }
      &SysUtils::reinsertAtMarker($pageRef, $nav, $marker);
    }
  }

  #--- insertPhotoNavigation main block

  my $album = shift;
  my $i = shift;
  my $pageRef = shift;
  my $template = shift;

  # Always insert the 'up' link (to the photo's album)
  my $photo = $album->{photos}->[$i];
  &insertPhotoNav($pageRef, $template->{navup}, '../index.html');

  # Insert a 'prev' link, unless this is the first photo
  my $href = undef;
  if ($i > 0) {
    $href = $album->{photos}->[$i-1]->{url_name} . '.html';
  }
  &insertPhotoNav($pageRef, $template->{navprev}, $href);

  # Insert a 'next' link, unless this is the last photo
  $href = undef;
  if ($i < (scalar(@{$album->{photos}}) - 1)) {
    $href = $album->{photos}->[$i+1]->{url_name} . '.html';
  }
  &insertPhotoNav($pageRef, $template->{navnext}, $href);
}

#--- Creates the actual image file for the given photo
#
#    void makeImageFile(hashref $photo)
#
sub makeImageFile {
  my $photo = shift;
  my $cmd;

  # Do not proceed if the image file exist, but only new
  # photos are to be processed
  unless (-e $photo->{photo_path} and $ONLY_NEW_PHOTOS) {

    # If the photo is actually a movie, use ffmpeg to
    # extract the first frame
    if ($photo->{is_movie}) {
      unlink($photo->{photo_path});
      $cmd = 'ffmpeg -i ' .
              '"' . $photo->{source_path} .'" ' .
              ' -ss 0 -vframes 1 -f image2 ' .
              '"' . $photo->{photo_path} . '" 2>&1 && ';
      $cmd .= 'mogrify ' .
            " -quality $QUALITY -resize $IMAGE_SIZE " .
            '"' . $photo->{photo_path} . '"';
    }
    # Normal photos will just be scaled down
    else {
      $cmd = 'convert ' .
             '"' . $photo->{source_path} . '"';
      if ($USE_SRGB_PROFILE) {
        $cmd .= " -profile '$SRGB_PROFILE_PATH'";
      }
      $cmd .= " -quality $QUALITY -resize $IMAGE_SIZE " .
              '"' . $photo->{photo_path} . '"';
    }
    # Call the external tool(s)
    my $out = qx($cmd);
    my $exitCode = $? >> 8;
    if ($exitCode != 0) {
      print STDERR ("Command '$cmd' exited with exit code $exitCode; output was:\n$out");
    }
  }
}

#--- Creates a thumbnail image for the given photo
#
#    void makeThumb(hashref $photo)
#
sub makeThumb {
  my $photo = shift;
  my $cmd;
  # Do not proceed if the thumbnail image exist, but only new
  # photos are to be processed
  if (not (-e $photo->{thumb_path} and $ONLY_NEW_PHOTOS)) {
    # If we already have a scaled version of the photo, use it
    if ($photo->{photo_path}) {
      # Build different command depending on whether or not
      # the thumbnails are to be cropped after scaling
      if ($CROP_THUMBNAILS) {
        $cmd = ' convert' .
               ' "' . $photo->{photo_path} . '"' .
               ' -resize x' . 2*$CROP_Y . ' -resize "' . 2*$CROP_X . 'x<" -thumbnail 50%' .
               " -gravity Center -crop $CROP_THUMBNAILS+0+0 +repage -quality $QUALITY "
      }
      else {
        $cmd = 'convert ' .
              '"' . $photo->{photo_path} . '"' .
              " -quality $THUMBNAIL_QUALITY -thumbnail $THUMBNAIL_SIZE ";
      }
      $cmd .= '"' . $photo->{thumb_path} . '"';
    }
    # We don't already have a scaled version of the photo
    else {
      # If it's a movie, use ffmpeg to extract the first frame
      if ($photo->{is_movie}) {
        unlink($photo->{thumb_path});
        $cmd = 'ffmpeg -ss 00:00:00:00 -t 00:00:00:01 -i ' .
                '"' . $photo->{source_path} .'"' .
                ' -f mjpeg ' .
                '"' . $photo->{thumb_path} . '" 2>&1 && ';
        if ($CROP_THUMBNAILS) {
          $cmd = 'mogrify' .
               ' -resize x' . 2*$CROP_Y . ' -resize "' . 2*$CROP_X . 'x<" -thumbnail 50%' .
               " -gravity Center -crop $CROP_THUMBNAILS+0+0 +repage -quality $QUALITY "
        }
        else {
          $cmd .= 'mogrify ' .
                " -quality $THUMBNAIL_QUALITY -thumbnail $THUMBNAIL_SIZE ";
        }
        $cmd .= '"' . $photo->{thumb_path} . '"';
      }
      # Otherwise just use the original photo for thumnail creation
      else {
        if ($CROP_THUMBNAILS) {
          $cmd = ' convert' .
                ' "' . $photo->{source_path} . '"' .
                ' -resize x' . 2*$CROP_Y . ' -resize "' . 2*$CROP_X . 'x<" -thumbnail 50%' .
                " -gravity Center -crop $CROP_THUMBNAILS+0+0 +repage -quality $QUALITY "
        }
        else {
          $cmd = 'convert ' .
                '"' . $photo->{source_path} . '"' .
                " -quality $THUMBNAIL_QUALITY -thumbnail $THUMBNAIL_SIZE ";
        }
        $cmd .= '"' . $photo->{thumb_path} . '"';
      }
    }
    # Call the external tool(s)
    my $out = qx($cmd);
    my $exitCode = $? >> 8;
    if ($exitCode != 0) {
      print STDERR ("Command '$cmd' exited with exit code $exitCode; output was:\n$out");
    }
  }
}

#--- Create image files, thumbnails and html files for all the
#    given album's photos and recurse into sub-albums
#
#    void makePhotos(hashref $album)
#
sub makePhotos {

  sub doMakePhotos {
    my $album = shift;
    my $nTotal = scalar(keys(%{$ALL_PHOTOS_INDEX}));
    &report('  Creating image files for album ' . $album->{url} . "\n");
    # Determine the path to this album's directory
    my $dir = "$TARGET_DIR$SysUtils::DIRSEP" . $album->{rel_path};
    # Iterate through all the album's photos
    my $nPhotos = scalar(@{$album->{photos}});
    for (my $i = 0; $i < $nPhotos; $i++) {
      my $photo = $album->{photos}->[$i];
      &report('    Creating image files for ' . $photo->{name} . ' [' . ($i+1) . " of $nPhotos in album / $nTotal total]\n");
      # Process photo file
      &makeImageFile($photo);
      &makeThumb($photo);
      # If inclusion of original files has been requested,
      # create a copy of this photos original file
      if ($INCLUDE_ORIGS or ($photo->{is_movie} and $INCLUDE_MOVIE_ORIGS)) {
        my $err = &SysUtils::copyFile($photo->{source_path}, $photo->{orig_path});
        if ($err) {
          print("Error while copying original file: $err");
        }
      }
    }
    # Recures into this album's sub-albums
    foreach my $child (@{$album->{children}}) {
      &doMakePhotos($child);
    }
  }

  my $root = shift;
  # Do not process photos if not requested
  unless ($DONT_RECREATE) {
    &report('Processing a total of ' . scalar(keys(%{$ALL_PHOTOS_INDEX})) . " image files...\n");
    &doMakePhotos($root);
  }

}


#--- Uses the general methods described above to extract all
#    special tag pairs from the album page tempate and
#    returns a hashref containing further hashrefs which
#    contain the actual parsing results:
#    - 'navs' => Descriptions of all album navigation sections,
#      i.e. links back to parent album(s)
#    - 'albums' => Descriptions of all sub-album list sections
#    - 'noalbums' => What to show if the album doesn't have
#      sub-albums
#    - 'photos' => Descriptions of all sections where thumbnail
#      previews of the album's photos are to be inserted
#    - 'nophotos' => What to show if the album doesn't contain
#      any photos
#
#    hashref $result parseAlbumTemplate(string $page)
#
sub parseAlbumTemplate {
  my $page = shift;

  my $result = {
    navs => {},
    albums => {},
    thumbs => {}
  };
  my @tmp = ();

  # Find al <@nav>...</@nav> pairs
  my $navs = &parseSections(\$page, 'nav');
  if (scalar(keys(%{$navs})) == 0) {
    die('No <@nav>...</@nav> section found in album template!');
  }
  # Process all 'nav' sections
  foreach my $marker (keys(%{$navs})) {
    my $section = $navs->{$marker};
    # Determine how to format parent album link...
    my $item = &parseSingleSection(\$section, 'navitem');
    my $links = &parseLinks(\$item->{section});
    # ...and what to put inbetween
    my $sep = &parseSingleSection(\$section, 'navsep');
    &SysUtils::reinsertAtMarker(\$section, '', $sep->{marker});
    # Compile that information...
    my $navDescr = {
      item => $item,
      links => $links,
      section => $section,
      sep => $sep->{section}
    };
    # ...and store it in the 'navs' hash, using the current
    # 'nav' section's marker as key
    $result->{navs}->{$marker} = $navDescr;
  }

  # Process all 'albums' sections
  my $albums = &parseSections(\$page, 'albums');
  foreach my $marker (keys(%{$albums})) {
    my $section = $albums->{$marker};
    # Determine how to format individual sub-album entries
    my $item = &parseSingleSection(\$section, 'album');
    # Therein, figure out what to insert if no thumb is available...
    my $nothumbs = &parseSections(\$item->{section}, 'nothumb');
    # ...how to format the actual link...
    my $links = &parseLinks(\$item->{section});
    # ...and how the album's thumb is to look like (if available)
    my $thumbs = &parseSections(\$item->{section}, 'thumb');
    # Compile that information...
    my $albumDescr = {
      item => $item,
      links => $links,
      nothumbs => $nothumbs,
      section => $section,
      thumbs => $thumbs
    };
    # ...and store it in the 'albums' hash, using the current
    # 'albums' section's marker as key
    $result->{albums}->{$marker} = $albumDescr;
  }

  # Find out what to show if an album doesn't have sub-albums
  my $noalbums = &parseSections(\$page, 'noalbums');
  foreach my $marker (keys(%{$noalbums})) {
    $result->{noalbums}->{$marker} = $noalbums->{$marker}
  }

  # Process all 'photos' sections
  my $photos = &parseSections(\$page, 'photos');
  foreach my $marker (keys(%{$photos})) {
    my $section = $photos->{$marker};
    # Determine how to format individual photos...
    my $item = &parseSingleSection(\$section, 'photo');
    # ...and how to format links to its html page
    my $links = &parseLinks(\$item->{section});
    # Compile that information...
    my $photoDescr = {
      item => $item,
      links => $links,
      section => $section
    };
    # ...and store it in the 'photos' hash, using the current
    # 'photos' section's marker as key
    $result->{photos}->{$marker} = $photoDescr;
  }

  # Find out what to show if an album doesn't contain any photos
  my $nophotos = &parseSections(\$page, 'nophotos');
  foreach my $marker (keys(%{$nophotos})) {
    $result->{nophotos}->{$marker} = $nophotos->{$marker}
  }

  # Store the parsed page template
  $result->{page} = $page;
  return $result;
}

#--- Create the given album's html files using the given template;
#    recursively do the same for its sub-albums
#
#    void makeAlbumPages(hashref $album, stringref $template)
#
sub makeAlbumPage {
  my $album = shift;
  my $template = shift;

  # The directory holding the ablum's gallery files
  my $dir = "$TARGET_DIR$SysUtils::DIRSEP" . $album->{rel_path};
  my $albumPath = "$dir${SysUtils::DIRSEP}index.html";

  # Get a copy of the parsed template...
  my $page = $template->{page};
  # Fill in the curren album's data into the corresponding
  # previously marked sections
  &insertAlbumNavigation($album, \$page, $template);
  &insertSubAlbums($album, \$page, $template);
  &insertPhotoThumbs($album, \$page, $template);
  &insertObjectInfo($album, \$page);
  &insertGeneralInfo($album, \$page);
  &processEvalTags($album, \$page);
  # Finally write the album's hatml file to disk
  &SysUtils::writeToFile($albumPath, $page);

  # Recursively process the current album's sub-albums
  foreach my $child (@{$album->{children}}) {
    &makeAlbumPage($child, $template);
  }
}

#--- Inserts into the given html page(ref) navigation elements
#    for the given album as described in the parsed template
#
#    void insertAlbumNavigation(hashref $album,
#                               stringref $pageRef,
#                               hashref $template)
#
sub insertAlbumNavigation {
  my $album = shift;
  my $pageRef = shift;
  my $template = shift;

  # Iterate through all navigation sections
  my $navs = $template->{navs};
  foreach my $marker (keys(%{$navs})) {
    # Find out how the navigation section is to look
    # like in general...
    my $itemtemplate = $navs->{$marker}->{item}->{section};
    # ...its marker within the parsed page...
    my $itemmarker = $navs->{$marker}->{item}->{marker};
    # ...where to insert links...
    my $itemlinks = $navs->{$marker}->{links};
    # ...and what to put between links
    my $itemsep = $navs->{$marker}->{sep};
    # Just in case someone wants to insert inormation on
    # the current album into the link separator
    &insertObjectInfo($album, \$itemsep);
    &insertGeneralInfo($album, \$itemsep);

    # Building the html text for the navigation section
    my $items = "";
    my $parent = $album;
    # As long we haven't reached the album tree's root,
    # we have to insert links to super-albums
    while (defined($parent)) {
      # Get a copy of the super-album link template
      my $item = $itemtemplate;
      # Determine relative path to super-album
      my $path = '../' x ($album->{level} - $parent->{level});
      my $href;
      # Don't insert a link in the first loop in which
      # we are still dealing with the current album
      if ($parent != $album) {
        $href="${path}index.html";
      }
      # Insert the constructed link as well as information
      # on the current super-album
      &reinsertLinks(\$item, $itemlinks, $href);
      &insertObjectInfo($parent, \$item);
      &insertGeneralInfo($album, \$item);
      # Execute any embedded Perl code
      &processEvalTags($album, \$item, $parent);

      # Prepend already existsing links with
      # the current link
      $items = $item . $items;

      # Unless we have reached the root...
      $parent = $parent->{parent};
      if (defined($parent)) {
        # ...process and prepend the separator
        my $is = $itemsep;
        &processEvalTags($album, \$is, $parent);
        $items = $is . $items;
      }
    }
    # Insert the constructed navigation element into the
    # page template
    my $section = $navs->{$marker}->{section};
    &SysUtils::reinsertAtMarker(\$section, $items, $itemmarker);
    &SysUtils::reinsertAtMarker($pageRef, $section, $marker);
  }
}

#--- Into the given html page(ref), insert all the album's
#    sub-albums formatted as described in the given template
#
#    void insertSubAlbums(hashref $album,
#                         stringref $pageRef,
#                         hashref $template)
#
sub insertSubAlbums {
  my $album = shift;
  my $pageRef = shift;
  my $template = shift;

  # Iterate through all 'albums' sections
  my $albums = $template->{albums};
  foreach my $marker (keys(%{$albums})) {
    # Get the current 'albums' section
    my $section = $albums->{$marker}->{section};
    # Find out how to format each sub-album item...
    my $item = $albums->{$marker}->{item};
    # ...how links to a sub-album are to look like...
    my $links = $albums->{$marker}->{links};
    # ...how to insert album thumbnails...
    my $thumbs = $albums->{$marker}->{thumbs};
    # ...and what to insert if no thumb is available
    my $nothumbs = $albums->{$marker}->{nothumbs};

    # If this album does have sub-albums
    if (scalar(@{$album->{children}}) > 0) {
      # This is going to be the current sub-ablums html text
      my $items = "";
      # Iterate through all the current album's sub-albums
      foreach my $child (@{$album->{children}}) {
        # Get the template for a single sub-album item
        my $subalbum = $item->{section};
        # Depending on whether or not a thumb is available...
        if ($child->{thumb}) {
          # ...insert it....
          foreach my $tmarker (keys(%{$thumbs})) {
            &SysUtils::reinsertAtMarker(\$subalbum, $thumbs->{$tmarker}, $tmarker);
          }
          foreach my $tmarker (keys(%{$nothumbs})) {
            &SysUtils::reinsertAtMarker(\$subalbum, '', $tmarker);
          }
        }
        else {
          # ...or display the 'nothum' text instead
          foreach my $tmarker (keys(%{$thumbs})) {
            &SysUtils::reinsertAtMarker(\$subalbum, '', $tmarker);
          }
          foreach my $tmarker (keys(%{$nothumbs})) {
            &SysUtils::reinsertAtMarker(\$subalbum, $nothumbs->{$tmarker}, $tmarker);
          }
        }
        # Insert the actual link to the sub-album's html page
        &reinsertLinks(\$subalbum, $links, $child->{url_name}. '/index.html');
        # If available, insert the thumbnail's ulr into <img src="...">
        if ($child->{thumb}) {
          $subalbum =~ s/%i/$child->{url_name}\/thumb.jpg/;
        }
        # Insert further information on the current sub-album
        &insertObjectInfo($child, \$subalbum);
        &insertGeneralInfo($album, \$subalbum);
        &processEvalTags($album, \$subalbum, $child);

        # Add the current sub-album to the album list
        $items .= "$subalbum\n";
      }
      # Insert the finished album list into the current 'albums' section
      SysUtils::reinsertAtMarker(\$section, $items, $item->{marker});
    }
    # If the current album doesn't have any sub-albums, replace
    # the whole section with an empty string
    else {
      $section = '';
    }
    # Insert the finished 'albums' section into the page template
    SysUtils::reinsertAtMarker($pageRef, $section, $marker);
  }

  # Now process all 'noalbums' sections
  my $noalbums = $template->{noalbums};
  foreach my $marker (keys(%{$noalbums})) {
    # If the current album does have sub-albums, insert an
    # empty string...
    if (scalar(@{$album->{children}}) > 0) {
      SysUtils::reinsertAtMarker($pageRef, '', $marker);
    }
    # ...and insert the 'noalbums' section's text otherwise
    else {
      my $noalbum = $noalbums->{$marker};
      &insertObjectInfo($album, \$noalbum);
      &insertGeneralInfo($album, \$noalbum);
      &processEvalTags($album, \$noalbum);
      SysUtils::reinsertAtMarker($pageRef, $noalbum, $marker);
    }
  }
}


#--- Into the given html page(ref), insert thumbnails for all
#    photos contained within the given album,formatted as
#    described in the given template
#
#    void insertPhotoThumbs(hashref $album,
#                           stringref $pageRef,
#                           hashref $template)
#
sub insertPhotoThumbs {
  my $album = shift;
  my $pageRef = shift;
  my $template = shift;

  # Iterate through all 'photos' sections
  my $photos = $template->{photos};
  foreach my $marker (keys(%{$photos})) {
    # Get the current 'photos' section
    my $section = $photos->{$marker}->{section};
    # Figure out how to format individual thumbnails...
    my $item = $photos->{$marker}->{item};
    # ...and how to create links to the photo viewing pages...
    my $links = $photos->{$marker}->{links};

    # If the album does contain photos...
    if (scalar(@{$album->{photos}}) > 0) {
      # This will contain the thumbnail's html text
      my $items = "";
      # Iterate through all photos
      foreach my $photo (@{$album->{photos}}) {
        # Get the template for a single photo
        my $ph = $item->{section};
        # Insert href to photo viewing page
        &reinsertLinks(\$ph, $links, 'photos/' . $photo->{url_name} . '.html');
        # Insert href for <img src="..."> tag
        $ph =~ s/%i/thumbs\/$photo->{photo_name}/g;
        # Insert furhter information on curren photo
        &insertObjectInfo($photo, \$ph);
        &insertGeneralInfo($album, \$ph);
        &processEvalTags($album, \$ph, $photo);
        # Add to html text
        $items .= $ph;
      }
      # Insert the finished 'photos' section into the page template
      &SysUtils::reinsertAtMarker($pageRef, $items, $marker);
    }
    # If the album doesn't contain any photos...
    else {
      # ...just blank the 'photos' section
      &SysUtils::reinsertAtMarker($pageRef, '', $marker);
    }
  }

  # Now process all 'nophotos' sections
  my $nophotos = $template->{nophotos};
  foreach my $marker (keys(%{$nophotos})) {
    # If the album does contain any photos, insert
    # an empty string...
    if (scalar(@{$album->{photos}}) > 0) {
      SysUtils::reinsertAtMarker($pageRef, '', $marker);
    }
    # ...and insert the section's text otherwise
    else {
      my $nophoto = $nophotos->{$marker};
      &insertObjectInfo($album, \$nophoto);
      &insertGeneralInfo($album, \$nophoto);
      &processEvalTags($album, \$nophoto);
      SysUtils::reinsertAtMarker($pageRef, $nophoto, $marker);
    }
  }

}

#--- Replaces certain '%somechar' markers within the given
#    string(ref) with information on the given object (either
#    a photo or an album hashref). Usually called with an album
#    objects only during sub-album processing and creation of
#    album navigation elements; into album viewing pages,
#    information on the corresponding album usually gets
#    inserted using the insertGeneralInfo function below.
#
#
#    void insertObjectInfo(hashref $object, stringref $strRef)
#
sub insertObjectInfo {
  my $object = shift;
  my $strRef = shift;

  # Common information for  both album and photo objects
  #
  # %c => the object's caption
  # %m => the object's name
  # %x => the object's id
  #
#  ${$strRef} =~ s/%c/($object->{caption} ? &toHTML($object->{caption}) : '')/ge;
  ${$strRef} =~ s/%c/($object->{caption} ? &toHTML($object->{caption}) : '')/ge;
  ${$strRef} =~ s/%m/($object->{name} ? &toHTML($object->{name}) : '')/ge;
  ${$strRef} =~ s/%x/($object->{id} ? &toHTML($object->{id}) : '')/ge;

  # Information available for albums
  #
  # %d => date of album creation
  # %N => number of photos contained in the album
  #
  if ($object->{photos}) {
    ${$strRef} =~ s/%d/($object->{date} ? &toHTML($object->{date}) : '')/ge;
    ${$strRef} =~ s/%M/&toHTML($object->{total_photos})/ge;
    ${$strRef} =~ s/%N/&toHTML(scalar(@{$object->{photos}}))/ge;
  }

  # Information available for photos
  #
  # %i => the photo file's name
  # %d => the photo's date and time of creation
  # %n => the photo's index number (after sorting) within the album
  # %N => the number of photos contained in this photo's album
  #
  else {
    ${$strRef} =~ s/%i/&toHTML($object->{photo_name})/ge;
    ${$strRef} =~ s/%d/($object->{datetime} ? &toHTML($object->{datetime}) : '')/ge;
    ${$strRef} =~ s/%n/($object->{n} ? &toHTML($object->{n}) : '')/ge;
    ${$strRef} =~ s/%N/&toHTML(scalar(@{$object->{album}->{photos}}))/ge;
  }
}

#--- Inserts into the given html page(ref) some general information
#    based on the given album and, optionally, a photo hashref
#
#    void insertGeneralInfo(hashref $album,
#                           stringref $pageRef
#                           [, hashref $data])
#
sub insertGeneralInfo {
  my $album = shift;
  my $pageRef = shift;
  # Optional data
  my $data = undef;
  if (@_) {
    $data = shift;
  }

  # Initialise html page title with the album's name
  my $title = $album->{name};
  # Initialize a relative linkt back to the root album's
  # directory with the album's base url
  my $baseLink = $album->{base_url};
  # If a photo object has been passed as additional data,
  # we assume that we are processing a photo viewing page.
  # In this case...
  if (defined($data) && !$data->{photos}) { # data is a photo object
    # ...append the photo's name to the html page title...
    $title = $title . ' - ' . $data->{name};
    # ...and add another parent dir level to the relative link
    # towards the root album's directory
    $baseLink .= '../';
  }

  # General information finally inserted:
  #
  # %t => html page title, constructed from the album's (and, if
  #       given, photo's) name
  # %s => link to stylesheet 'styles.css' in root album's directory
  # %b => relative link to root album's directory
  #
  ${$pageRef} =~ s/%t/$title/g;
  ${$pageRef} =~ s/%s/${baseLink}styles.css/g;
  ${$pageRef} =~ s/%b/$baseLink/g;
}

#--- Executes by eval() perl code enclosed in <@eval>...</@eval>
#    tags within the given string(ref). Passing an album hashref
#    is mandatory; optional data (usually a photo hashref) may be
#    provided
#
#    void processEvalTags(hashref $album,
#                         stringref $strRef
#                         [, hashref $data])
#
sub processEvalTags {
  my $album = shift;
  my $strRef = shift;
  # Optional data
  my $data = undef;
  if (@_) {
    $data = shift;
  }
  # Find all 'eval' sections and iterate through them
  my $evals = &parseSections($strRef, 'eval');
  foreach my $marker (keys(%{$evals})) {
    my $eval = $evals->{$marker};
    # Within the current 'eval' section, process 'str' tags
    &processStrTags(\$eval);
    # The actual Perl code evaluation
    my $result = eval($eval);
    # If an error occured...
    if ($@) {
      # print out a warning and insert an empty string...
      print("Warning: execution of\n$eval\nreturned with error: $@\n");
      &SysUtils::reinsertAtMarker($strRef, '', $marker);
    }
    else {
      # ...and insert the evaluation result if everything went well
      &SysUtils::reinsertAtMarker($strRef, $result, $marker);
    }
  }
}

#--- Within the given html page(ref), escape the text contents
#    of each <@str>...</@str> pair so that it could be used
#    as a double-quoted string literal.
#
#    void processStrTag(stringref $strRef)
#
sub processStrTags {
  my $strRef = shift;
  # Find all 'str' sections and iterate through them
  my $strs = &parseSections($strRef, 'str');
  foreach my $marker (keys(%{$strs})) {
    # Replace contents with qq-escaped version of itself
    my $str = $strs->{$marker};
    $str = &SysUtils::qqEscape($str, '$', '@');
    &SysUtils::reinsertAtMarker($strRef, $str, $marker);
  }
}

#--- For the given album, create a thumbnail image; recursively
#    proceed with its sub-albums
#
#    void makeAlbumThumbs(hashref $album)
#
sub makeAlbumThumbs {

  #--- Here the actual album processing for thumbnail
  #    creation happens
  #
  #    void doMakeAlbumThumbs(hashref $album)
  #
  sub doMakeAlbumThumbs {
    my $album = shift;
    # If the album does have a thumbnail definition...
    if ($album->{thumb}) {
      # ...create the corresponding image file
      &report('  Creating thumbnail for album ', $album->{url}, "\n");
      &makeThumb($album->{thumb});
    }
    else {
      &report('  [Album ', $album->{url}, " doesn't have a thumbnail!]\n");
    }

    # Recure into sub-albums
    foreach my $child (@{$album->{children}}) {
      &doMakeAlbumThumbs($child);
    }
  }

  #--- makeAlbumThumbs main block

  my $album = shift;

  # Do not proceed if recreation of image files has been forbidden
  unless ($DONT_RECREATE) {
    &report('Creating a total of ' . scalar(@{$wantedAlbums}) . " album thumbnails...\n");
    &doMakeAlbumThumbs($album);
  }
}



#--- I/O stuff --------------------------------------------------------


#--- Determines the dimensions (width, height) of
#    the image file specified by the given path using
#    ImageMagick's 'identify' command
#
#    (int $width, int $height) getDimensions(string $path)
#
sub getDimensions {
  my $path = shift;

  my $width = 0;
  my $height = 0;
  my $cmd = "identify \"$path\"";
  my $out = qx($cmd);
  # From identify's output, try to parse image dimensions
  if ($out =~ m/[A-Z]+\s+(\d+)x(\d+)\s/) {
    $width = $1;
    $height = $2;
  }
  return ($width, $height)
}

#--- Prints out all given argument if verbosity has been requested
#
#    void report(list @_)
#
sub report {
  if ($VERBOSE) {
    print(@_);
  }
}

#--- Converts all non-html characters within the given string
#    into html escape sequences and replaces linebreaks with
#    <br> tags
#
#    string toHTML(string $str)
#
sub toHTML {
  my $str = shift;
  $str = &SysUtils::htmlEscape($str);
  $str =~ s/\n/<br>/g;
  return $str;
}

#--- Prepares the given string for usage as an SQLite
#    string literal by escaping some characters -- namely:
#
#    single quote => two consecutive single quotes
#    <linebreak> => '\n'
#    <tab> => '\t'
#
#    string sqlEscape(string $str)
#
sub sqlEscape {
  my $str = shift;
  $str =~ s/'/''/g;
  $str =~ s/\\n/\n/g;
  $str =~ s/\\t/\t/g;
  return $str;
}

#--- Versatile method for retrieving data from a given
#    digikam.db table.
#
#    listref<hashref> getRecords(
#      string $table
#      [, arrayref $cols]
#      [, hashref $cond]
#      [, string $addCond
#         [, string $append]]
#    )
#
#    $table: The name of the table to query
#    $cols: Reference to a list containing the names of the
#           columns to return; default is '*'
#    $conds: Reference to a hash containing pairs of column
#            names and literals, which will be converted to
#            equality conditions key="value" joined by AND and
#            used as WHERE clause
#    $addCond: String appended to the WHERE clause (or as *the*
#              WHERE clause if no $conds have been given)
#    $append: String to finally append to the SQL statement
#             constructed so far; typically something like
#             'ORDER BY'
#
#    The optional arguments may be given in any order.
#    String literals must already be escaped using the
#    sqlEscape function described above.
#
#    Example:
#
#    my $albumInfo = &getRecords('Albums',
#                                ['id', 'name'],
#                                {collection => 'Flowers'},
#                                'AND url like "/Travel%"',
#                                'ORDER BY name
#
#    translates to SQL as follows:
#
#    SELECT id, name FROM Albums WHERE collection="Flowers"
#    AND url like "/travel%' ORDER BY name
#
sub getRecords {
  my $table = shift;
  my $cols = ['*'];
  my $cond = {};
  my $addCond = undef;
  my $append = undef;

  # Process arguments
  while (scalar(@_) > 0) {
    my $arg = shift;
    if (defined($arg)) {
      if (ref($arg) eq 'ARRAY') {
        $cols = $arg;
        next;
      }
      elsif (ref($arg) eq 'HASH') {
        $cond = $arg;
        next;
      }
    }
    if (defined($addCond)) {
      $append = $arg;
    }
    else {
      $addCond = (defined($arg) ? $arg : '');
    }
  }

  # Create the SELECT <columns> FROM <table> part
  my $stm = 'PRAGMA short_column_names = false; PRAGMA full_column_names = true; SELECT ' . join(',', @{$cols}) . " FROM $table";

  # Create the <key=value> [AND <key=value> part of WHER
  my @keys = keys(%{$cond});
  if (scalar(@keys) > 0) {
    my @conds = ();
    foreach my $key (@keys) {
      push (@conds, "$key=\"" . $cond->{$key} . "\"");
    }
    $stm = $stm . ' WHERE ' . join(' AND ', @conds);
  }
  # Append additional conditions to WHERE
  if (defined($addCond) and ($addCond ne '')) {
    if (scalar(@keys) == 0) {
      $stm = "$stm WHERE";
    }
    $stm = "$stm $addCond";
  }

  # Add the last fragment, if given (e.g. ODER BY)
  if (defined($append) and ($append ne '')) {
    $stm .= " $append";
  }

  # Dont's forget the trailing semicolon...
  $stm .= ';';

  # Escape all double quotes
  $stm =~ s/"/\\"/g;

  if ($DEBUG) {
    print("SQL: $stm\n");
  }

  # Build and execute a command line string asking the sqlite3
  # client to execute the constructed SQL statement; capture
  # the output. This is merely for 1. figuring out whether we
  # find any records at all and 2) determining the order and names
  # of returned columns
  my $cmd = "sqlite3 -header -list -separator '\t' \"$DIGIKAM_DB\" \"" . $stm . '"';
  my @out = qx($cmd);
  chomp(@out);

  # Crash if sqlite3 returned with an error
  my $exitCode = $? >> 8;
  if ($exitCode != 0) {
    die("Database access error!");
  }

  # Build the list of hash representations of the
  # retrieved recdords
  my @records = ();
  if (scalar(@out) > 0) {
    # Identify names and order of returned columns
    my $headers = shift(@out);
    my @colNames = split(/\t/, $headers);
    # Find column names which would be ambigious
    # if not qualified with table name
    my %AMBIG_COLS = ();
    foreach my $colName (@colNames) {
      if ($colName =~ m/\w+\.(\w+)/) {
        if (exists($AMBIG_COLS{$1})) {
          $AMBIG_COLS{$1} = 1;
        }
        else {
          $AMBIG_COLS{$1} = 0;
        }
      }
    }
    my $nCols = scalar(@colNames);
    my $colNamesRegex = join('|', @colNames);

    # Call sqlite3 again, but this time ask for the output
    # to be formatted in a way so that we can also parse fields
    # containing line breaks
    $cmd = "sqlite3 -line $DIGIKAM_DB \"" . $stm . '"';
    @out = qx($cmd);
    chomp(@out);

    # Crash on sqlite3 error
    my $exitCode = $? >> 8;
    if ($exitCode != 0) {
      die("Database access error!");
    }

    # Prepare for building record hashes
    my $record = {}; # For constructing record hashes
    my $found = 0;   # Number of seen column names
    my $key = undef; # Currently processed column name
    my $val = undef; # For assembling the current field value
    my $last = undef;# Last seen part of the current field's value

    # Iterate through all lines of output
    foreach my $line (@out) {
      # If we find a line starting with a column name,
      # followed by an equals sign and (possibly) some
      # more characters, we've found the beginning of a
      # field value
      if ($line =~ m/^\s*($colNamesRegex)\s=\s(.*)$/) {
        # If we've already seen the expected number of
        # fields, store and reset the record hash
        if ($found == $nCols) {
          &cleanupColNames($record, %AMBIG_COLS);
          push(@records, $record);
          $record = {};
          $found = 0;
        }

        # On any account, store the identified column
        # name / field value pair into the record hash
        $key = $1;
        $val = $2;
        $record->{$key} = $val;
        # Increase the number of seen fields
        $found++;
        $last = undef;
      }
      # No new field value started => we've found a field
      # value containing line breaks
      else {
        # If we haven't seen enough column names yet...
        if ($found < $nCols) {
          # ...just append the current line to the
          # currently processed field's value
          $record->{$key} .= "\n$line";
        }
        # Seems we're processing the last missing field
        # value
        else {
          # This this is the second, third etc. line of the last
          # missing field (the first line has already been
          # processed in the if-branch way above).
          #
          # Since this could always be the blank line separating
          # the the current record from the next one, we don't
          # append it immediately, but store it in $last
          # so that we can append it if we arrive here
          # again (and it apparently hadn't been the blank
          # separation line)
          #
          if (defined($last)) {
            $record->{$key} .= "\n$last";
          }
          $last = $line;
        }
      }
    }
    # Don't discard the last record!
    if ($found == $nCols) {
      &cleanupColNames($record, %AMBIG_COLS);
      push(@records, $record);
    }
  }
  return \@records;
}

#--- Removes table names from non-ambigious column names
#    of given record
#
#    cleanupColNames(hashref $record, %ambigCols)
#
sub cleanupColNames {
  my $record = shift(@_);
  my %ambigCols = @_;
  foreach my $key (keys(%{$record})) {
    if ($key =~ m/\w+\.(\w+)/) {
      unless ($ambigCols{$1}) {
        my $val = $record->{$key};
        delete($record->{$key});
        $record->{$1} = $val;
      }
    }
  }
}


#=== Default templates ================================================

#--- From the passed html page, remove all html comments
#
#    string removeHTMLComments(string $html)
#
sub removeHTMLComments {
  my $html = shift;
  $html =~ s/\<!--.*?--\>//gs;
  return $html;
}

#--- Returnes the built-in album page template. Call
#    digikam2web.pl -A to have it printed out to STDOUT,
#    from where you can re-direct it towards a file for
#    further examination and editing.
#
#    string getDefaultAlbumTemplate()
#
sub getDefaultAlbumTemplate {
  my $template = <<EOT;
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
  "http://www.w3.org/TR/html4/loose.dtd">
<html>

  <!--

    Default album page template for digikam2web.pl vers. $VERSION

    General placeholders:
    %b - will be replaced with a (relative) link to the gallery's
         base directory; can e.g. be used for constructing links
         to images like <img src="%bleft_arrow.png">
    %s - will be replaced with a (relative) link to the gallery's
         stylesheet

    The following placeholders will be replaced with album-specific
    values:
    %t - will be replaced by a title generated from the album's
         directory name
    %c - will be replaced with the album comment (or the album's
         directory name, if there is no comment)
    %d - will be replaced with the album's date of creation
    %i - will be replaced with the path to the album's thumbnail image
    %m - will be replaced with the album's directory name
    %M - will be replaced with the number of photos contained in
         the album and (recursiveley) all its sub-albums
    %N - will be replaced with the number of photos contained in
         the album
    %x - will be replaced with the album's internal id

    Within <\@navitem>...</\@navitem> and <\@album>...</\@album>
    sections (see below), the corresponding super- or sub-album's
    data will be inserted.

  -->

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <title>%t (%d)</title>
    <link rel="stylesheet" type="text/css" href="%s">
  </head>
  <body class="album">
    <div class="nav">

      <!--

        The <\@nav>...</\@nav> section will contain
        links for navigating towards albums further
        *up* in the album tree.

        The mandatory <\@navitem>...</\@navitem> section defines
        how individual navigation elements get constructed.
        In particular, each containing <\@link>...</\@link>
        pair will be replaced with a pair of <a href=...>...</a>
        tags referencing the respective album page.

        Furthermore, each "nav" section must contain exactly one
        <\@navsep>...</\@navsep> item which defines what will
        be inserted *between* individual navigation links.

        An album template may contain multiple "nav" sections.

      -->

      <\@nav>
        <\@navitem><\@link>\%c (\%d)</\@link></\@navitem>
        <\@navsep>&nbsp;&gt;&gt;&nbsp;</\@navsep>
      </\@nav>
    </div>

    <!--

      <\@albums>...</\@albums> sections contain navigation items
      pointing towars the current album's sub-albums. Each item will
      be created according to the definition enclosed by a mandatory
      <\@album>...</\@album> section. Actual link will be created by
      replacing <\@link>...</\@link> pairs with appropriate
      <a href=...>...</a> tags.

      An "album" section may contain <\@thumb>...</\@thumb> sections
      the content of which will be retained if a thumbnail is available
      for the respective album (the actual "thumb" tags will be removed).
      Otherwise the whole "thumb" section will be deleted.

      <\@nothumb>...</\@nothumb> sections are treated just the opposite
      way: they will be deleted if a thumbnail is available and will
      be retained if not.

    -->

    <\@albums>
      <div class="albums">
        <ul>
          <\@album><li><\@thumb><\@link><img src="\%i"></\@link></\@thumb><\@nothumb>[No thumb] </\@nothumb><\@link>\%c (\%M Photos)</\@link></li>
          </\@album>
        </ul>
      </div>
    </\@albums>

    <!--

      If an album doesn't contain any sub-albums, no "albums"
      section will be inserted; in this case, the content of any
      <\@noalbums>...</\@noalbums> section will be displayed
      (emtpy in this example).

    -->

    <\@noalbums></\@noalbums>

    <div class="photos">
      <ul>

        <!--

          The <\@photos>...</\@photos> sections contains a
          collection of thumbnails of the photos stored in
          the current album, each being represented by the
          definition enclosed in a <\@photo>...</\@photo>
          pair. Links to the individual photo pages are inserted
          using <\@link>...</\@link> tags, which will be
          replaced with approriate <a href=...>...</a> pairs.

          N.B: In this section, placeholders like \%c, \%d or
          \%m will refer to the photos' properties, rather than
          to the current albums'.

        -->

        <\@photos>
          <\@photo><li><\@link><img src="\%i"></\@link><br><\@link><\@eval>
          <!--

            This "photo" section demonstrates the usage of
            <\@eval>...</\@eval> tags. Perl code embraced by
            such an "eval" pair will be executed after all
            the current section's placeholders have been
            filled in, and the tag pair will be replaced by
            the string returned.

            In the (likely) case you want to access in your
            perl code the values filled in for placeholders,
            you are recommended to put the placeholder inside
            a <\@str>...</\@str> pair. This will cause the
            string substituded for the placeholder to be
            escaped as though it had been literally specified
            within double quotes (i.e. '\\' becomes '\\\\',
            '"' becomes '\\"', '\@' becomes '\\\@' etc.)
            prior to execution.

            From within the evaluated Perl code, a hashref
            \$album is accessable, which provides various
            information about the currently processed album.
            In many cases, an additional hashref called
            \$data is also available, which describes the
            properties of additional objects, e.g. the current
            sub- (in the "albums" list) or super-album
            (within "nav" sections), or the current photo
            (when in a "photos" section iterating through
            the photos stored in the current album). To get
            an idea about the available information, insert

            <\@eval>return &SysUtils::dumpRef(\$album, 0, 1)</\@eval>

            or

            <\@eval>return &SysUtils::dumpRef(\$data, 0, 1)</\@eval>

            respectively.

            When outputting "raw" values of the \$album or
            \$data hashref, make sure you run the dat through
            the &SysUtils::htmlEscape function (as seen below).

          -->
my \$caption = \$data->{caption};
if (!\$caption) {
  return "&nbsp;";
}
elsif (\$caption =~ m/^(.{1,10}).+/) {
  return &SysUtils::htmlEscape("\$1...");
}
else {
  return "<\@str>\%c</\@str>"
}
          </\@eval></\@link></li>
          </\@photo>
        </\@photos>

        <!--

          If an album doesn't contain any photos, no "photos"
          section will be inserted; in this case, the content of any
          <\@nophotos>...</\@nophotos> section will be displayed
          instead.

        -->

        <\@nophotos><li>[No photos in \%m]</li></\@nophotos>
      </ul>
    </div>
  </body>
</html>
EOT
  return $template;
}


#--- Returnes the built-in photo page template. Call
#    digikam2web.pl -P to have it printed out to STDOUT,
#    from where you can re-direct it towards a file for
#    further examination and editing.
#
#    string getDefaultAlbumTemplate()
#
sub getDefaultPhotoTemplate {
  my $template = <<EOT;
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
  "http://www.w3.org/TR/html4/loose.dtd">
<html>

  <!--

    Default photo page template for digikam2web.pl vers. $VERSION

    General placeholders:
    %b - will be replaced with a link to the gallery's base
         directory; can e.g. be used for constructing links to
         images like <img src="%bimage.png">
    %s - will be replaced with a link to the gallery's stylesheet
    %t - will be replaced by a title generated from the album's
         directory name and the photo's file name

    The following placeholders will be replaced with photo-specific
    values:
    %c - will be replaced with the photos comment
    %d - will be replaced with the photos's date of creation
    %i - will be replaced with the path to the photo's actual
         image file
    %m - will be replaced with the photo file name
    %n - will be replaced with the current photo's index number
         within its album
    %N - will be replaced with the total number of photos contained
         in this photo's album
    %x - will be replaced with the photo's internal id

    Within <\@navitem>...</\@navitem> and <\@album>...</\@album>
    sections (see below), the corresponding super- or sub-album's
    data will be inserted.

    For a description of <\@eval>...</\@eval> and
    <\@str>...</\@str> tags see the default album template.

  -->

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>%t</title>
    <link rel="stylesheet" type="text/css" href="%s">
  </head>
  <body class="photo">

      <!--

        Each photo page should have at least one <\@navup>...</\@navup>,
        <\@navprev>...</\@navprev> and <\@navnext>...</\@navnext> section
        which will be replaced with navigation items pointing to the
        photo's album page, the previous photo and the next photo,
        respectively. Inside each "nav*" section, each <\@link>...</\@link>
        pair will be replace with appropriate <a href=...>...</a> tags.
        For the first image in an album, there is no previous photo
        to link to; likewise, there is no next photo for album's last
        image. In these cases, the contents of any <\@nolink>...</\@nolink>
        section will be displayed instead.

      -->

    <div class="nav" id="left">
      <\@navup><\@link>^</\@link></\@navup>
    </div>
    <div class="nav" id="right">
      <\@navprev><\@link>&lt;</\@link><\@nolink>&lt;</\@nolink></\@navprev>
      <\@navnext><\@link>&gt;</\@link><\@nolink>&gt;</\@nolink></\@navnext>
    </div>
    <div class="caption">&nbsp;%c&nbsp;</div>
    <div class="photo">

      <!--

        The actual image gets inserted by an <img> tag with a
        src attribute containing the \%i placeholder. Furhermore,
        any <\@orig>...</\@orig> pair will be replaced with a
        <a href=...>...</a> pair linking to the original (unscaled)
        photo or movie file, if requested by calling digikam2web.pl
        using the -o and/or -M command line switches.

      -->

      <div class="image"><\@orig><img src="\%i"></\@orig><p>\%m (\%d; \%n/\%N)</p></div>
    </div>
  </body>
</html>
EOT
  return $template;
}

#--- Returnes the built-in CSS stylesheet. Call
#    digikam2web.pl -Y to have it printed out to STDOUT,
#    from where you can re-direct it towards a file for
#    further examination and editing.
#
#    string getDefaultAlbumTemplate()
#
sub getDefaultStylesheet {
  my $stylesheet = <<EOT;
body {
  color: #000000;
  background-color: #fff8f0;
  padding: 0px;
  margin: 0px;
  font-family: Verdana,Helvetica,sans-serif;
  font-size: 12pt;
}

h1 {
  font-size: 12pt;
  padding: 8px;
}

a {
  color: #000040;
}

a:visited {
  color: #000040;
}

a:hover {
  color: #ff8000;
}

ul {
  margin: 0px;
  padding: 0px;
}

img {
  border: 1px solid #c0c0c0;
}

.nav {
  color: #ffffff;
  background-color: #000000;
  font-weight: bold;
  padding: 6px 12px 6px 12px;
  margin: 0px;
}

.nav a {
  color: #ffffff;
}

.nav a:visited {
  color: #ffffff;
}

.nav a:hover {
  color: #ffc000;
}

.album .nav {
}

.album .albums {
  background-color: #6090c0;
  padding: 6px 12px 6px 12px;
  margin: 0px;
}

.album .albums img {
    vertical-align: middle;
    margin: 0px 12px 0px 0px;
}

.album .albums li {
    display: block;
    clear: both;
    text-align: left;
    padding: 6px;
    margin: 12px 0px 12px 0px;
    background-color: #ffffff;
    border: 1px solid #c0c0c0;
}

.album .photos li {
    font-size: 10pt;
    display: block;
    float: left;
    text-align: center;
    margin: 6px;
    padding: 6px;
    background-color: #ffffff;
    border: 1px solid #c0c0c0;
}


.photo .nav a {
  text-decoration: none;
  color: #000040;
  background-color: #ffffff;
  padding-left: 2px;
  padding-right: 2px;
}

.photo .nav a:hover {
  background-color: #ffc000;
}

.photo .nav {
}

.photo #left {
  float: left;
}

.photo #right {
  float: right;
}

.photo .caption {
  font-weight: bold;
  color: #ffffff;
  text-align: center;
  background-color: #6090c0;
  padding: 6px 12px 6px 12px;
  margin: 0px;
}

.photo .image {
  text-align: center;
  padding: 12px;
  margin: 6px;
}
EOT
  return $stylesheet
}
