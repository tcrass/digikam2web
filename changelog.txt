CHANGELOG digikam2web.pl
========================


1.0.6
-----

- Changed -u option to actually convert images to sRGB; -U
  can be used to set a path to an sRGB ICC file.
- UTF-8 handling seems to finally work.


1.0.5
-----

- Added -u option to remove color profiles.
- Modified internal album template to use %t (%d) as the
  generated HTML pages' title.


1.0.4
-----

- And yet another attempt!


1.0.3
-----

- Yet another attempt to improve character encoding handling... sigh!


1.0.2
-----

- Added flv and ogg as video file extensions
- Updated ffmpeg command-line arguments for extracting 1st frame
  from video files to latest ffmpeg calling conventions

1.0.1
-----

- Fix: Errors when evaluating <str> tags enclosing text containing unescaped
  '$' and '@' characters
- Feature: Option to create Windows AUTORUN.INF file in target directory

1.0.0
-----

- Initial adaption to new database schema version (4) introduced with digikam 0.10
